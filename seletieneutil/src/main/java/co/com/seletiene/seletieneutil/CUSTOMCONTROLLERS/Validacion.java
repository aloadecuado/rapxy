package co.com.seletiene.seletieneutil.CUSTOMCONTROLLERS;

/**
 * Created by pedroalonsodazab on 17/08/17.
 */
public enum Validacion {
    AlphaNumeric,
    Alphabetic,
    Numeric,
    Email,
    CifraDinero,
    Decimal,
    Normal
}
