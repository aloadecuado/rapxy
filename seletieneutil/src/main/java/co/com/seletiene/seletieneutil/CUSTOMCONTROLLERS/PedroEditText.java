package co.com.seletiene.seletieneutil.CUSTOMCONTROLLERS;

import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;

import java.lang.reflect.Field;
import java.text.DecimalFormat;

import co.com.seletiene.seletieneutil.R;

import static co.com.seletiene.seletieneutil.CUSTOMCONTROLLERS.Validacion.CifraDinero;
import static co.com.seletiene.seletieneutil.CUSTOMCONTROLLERS.Validacion.Email;
import static co.com.seletiene.seletieneutil.CUSTOMCONTROLLERS.Validacion.Normal;


public class PedroEditText extends AppCompatEditText {



    public OnFocusChangeListener customListener;
    public Validacion TipoValidacion = Normal;
    public int RangoText = 200;

    private String Cifra = "";
    public Boolean Validado = false;
    private Boolean Editando = true;
    private Context Ctx;
    public PedroEditText(Context context) {
        super(context);
    }

    public PedroEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        Ctx = context;
        //ViewParent parentView = this.getParent();

        if (TipoValidacion != Email && TipoValidacion != CifraDinero)
        {
            this.setFilters(new InputFilter[]{
                    new InputFilter() {
                        public CharSequence filter(CharSequence src, int start,
                                                   int end, Spanned dst, int dstart, int dend) {

                            ViewParent parentView = PedroEditText.this.getParent().getParent();


                            if (RangoText <= dend) {
                                return "";
                            }
                            switch (TipoValidacion) {
                                case Normal:

                                    Validado = true;
                                    return src;
                                case AlphaNumeric:

                                    if (src.equals("")) { // for backspace
                                        Validado = true;
                                        return src;
                                    }
                                    if (src.toString().matches("[a-zA-Z0-9]+")) {
                                        Validado = true;
                                        return src;
                                    }
                                    return "";

                                case Alphabetic:

                                    if (src.equals("")) { // for backspace
                                        Validado = true;
                                        return src;
                                    }
                                    if (src.toString().matches("[a-zA-Z]+")) {
                                        Validado = true;
                                        return src;
                                    }
                                    return "";

                                case Numeric:

                                    if (src.equals("")) { // for backspace
                                        Validado = true;
                                        return src;
                                    }
                                    if (src.toString().matches("[0-9]+")) {
                                        Validado = true;

                                        return src;
                                    }
                                    return "";

                                case CifraDinero:


                                    if (src.toString().matches("[0-9]+")) {
                                        Validado = true;

                                        return src;
                                    }

                                    return "";


                                default:
                                    return src;
                            }

                        }
                    }
            });
        }


        this.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                ViewParent parentView = PedroEditText.this.getParent().getParent();

                if (parentView != null) {

                    switch (TipoValidacion) {
                        case CifraDinero:

                            Editando = true;
                            PedroTextInputLayout textInputEditText = (PedroTextInputLayout) parentView;
                            if (textInputEditText != null)
                            {

                                String NumericFormat = s.toString();
                                String number = NumericFormat;
                                try {
                                    double amount = Double.parseDouble(number);
                                    DecimalFormat formatter = new DecimalFormat("#,###");

                                    //System.out.println(formatter.format(amount));
                                    String ValorCifra = "Formato en pesos: $ " + formatter.format(amount);

                                    String hint = textInputEditText.getHint().toString();

                                    String[] sHints = hint.split("-");

                                    if (sHints.length >= 2)
                                    {
                                        hint = sHints[0];
                                    }

                                    textInputEditText.setHint(hint + "-" + ValorCifra);
                                }
                                catch (Exception e)
                                {
                                    double amount = Double.parseDouble("0");
                                    DecimalFormat formatter = new DecimalFormat("#,###");

                                    //System.out.println(formatter.format(amount));
                                    String ValorCifra = "Formato en pesos: $ " + formatter.format(amount);

                                    String hint = textInputEditText.getHint().toString();

                                    String[] sHints = hint.split("-");

                                    if (sHints.length >= 2)
                                    {
                                        hint = sHints[0];
                                    }

                                    textInputEditText.setHint(hint + "-" + ValorCifra);
                                }


                                //textInputEditText.setError(ValorCifra);
                            }


                            break;

                        case Email:

                            PedroTextInputLayout textInputEditText1 = (PedroTextInputLayout) parentView;

                            String email = PedroEditText.this.getText().toString().trim();

                            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                            if (email.matches(emailPattern) && s.length() > 0)
                            {
                                Validado = true;
                                textInputEditText1.setError("");
                            }
                            else
                            {
                                Validado = false;

                                textInputEditText1.setError("Esto no es un email");
                            }


                        break;
                    }
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ViewParent parentView = PedroEditText.this.getParent().getParent();
                Editando = false;

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

        });
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);

        if (customListener != null) {
            customListener.onFocusChange(this, focused);
        }
    }

    public PedroEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
