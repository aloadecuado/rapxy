package co.com.seletiene.seletieneutil.CUSTOMCONTROLLERS;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

/**
 * Created by pedroalonsodazab on 17/08/17.
 */

public class PedroTextInputLayout extends TextInputLayout implements View.OnClickListener{

    public OnFocusChangeListener customListener;
    public PedroTextInputLayout(Context context) {
        super(context);
    }

    public PedroTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);


        TextInputLayout textInputLayout = (TextInputLayout) this;
        //myTextInputLayout.add(textInputLayout);
        for( int j = 0; j < textInputLayout.getChildCount(); j++ )
        {
            if (((FrameLayout)textInputLayout.getChildAt(j)).getChildAt(0) instanceof EditText) {
                EditText edi = (EditText) ((FrameLayout) textInputLayout.getChildAt(j)).getChildAt(0);
            }
        }

    }

    public PedroTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean addViewInLayout(View child, int index, ViewGroup.LayoutParams params) {


        TextInputLayout textInputLayout = (TextInputLayout) this;
        //myTextInputLayout.add(textInputLayout);
        for( int j = 0; j < textInputLayout.getChildCount(); j++ )
        {
            if (((FrameLayout)textInputLayout.getChildAt(j)).getChildAt(0) instanceof EditText) {
                EditText edi = (EditText) ((FrameLayout) textInputLayout.getChildAt(j)).getChildAt(0);
            }
        }

        return super.addViewInLayout(child, index, params);
    }

    @Override
    public void onClick(View view) {
        TextInputLayout textInputLayout = (TextInputLayout) this;
        //myTextInputLayout.add(textInputLayout);
        for( int j = 0; j < textInputLayout.getChildCount(); j++ )
        {
            if (((FrameLayout)textInputLayout.getChildAt(j)).getChildAt(0) instanceof EditText) {
                EditText edi = (EditText) ((FrameLayout) textInputLayout.getChildAt(j)).getChildAt(0);
            }
        }

    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (customListener != null) {
            customListener.onFocusChange(this, focused);
        }
    }
}
