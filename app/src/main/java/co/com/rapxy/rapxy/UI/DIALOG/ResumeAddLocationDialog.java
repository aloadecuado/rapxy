package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;


/**
 * Created by pedrodaza on 5/02/18.
 */
@SuppressLint("ValidFragment")
public class ResumeAddLocationDialog  extends DialogFragment {

    public interface onGetUser
    {
        void onGetUser(UserObject userObject);
    }

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etAdrees)
    EditText etAdrees;

    @BindView(R.id.etTowel)
    EditText etTowel;

    @BindView(R.id.etFood)
    EditText etFood;

    AddresObject ADDRESOBJEC;
    UserObject USEROBJECT;

    onGetUser listener;
    @SuppressLint("ValidFragment")
    public ResumeAddLocationDialog(AddresObject addresObject, UserObject userObject, onGetUser onGetUser)
    {
        this.ADDRESOBJEC = addresObject;
        this.USEROBJECT = userObject;
        listener = onGetUser;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_resume_add_location, null);
        ButterKnife.bind(this, view);

        etName.setText(ADDRESOBJEC.getName());
        etAdrees.setText(ADDRESOBJEC.getAddres());
        etAdrees.setFocusable(false);
        etAdrees.setEnabled(false);

        builder.setView(view)


                // Add action buttons
                .setPositiveButton(R.string.btn_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        ADDRESOBJEC.setName(etName.getText().toString());
                        ADDRESOBJEC.setAddres(etAdrees.getText().toString());
                        ADDRESOBJEC.setTowell(etTowel.getText().toString());
                        ADDRESOBJEC.setFoot(etFood.getText().toString());
                        new SetUserClass().addAdreesUserForId(USEROBJECT, ADDRESOBJEC, new SetUserClass.onGetUser() {
                            @Override
                            public void onGetUser(UserObject userObject) {
                                listener.onGetUser(userObject);
                            }
                        });


                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ResumeAddLocationDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
