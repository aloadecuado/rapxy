package co.com.rapxy.rapxy.UTIL;

import android.app.ProgressDialog;
import android.content.Context;

import java.util.List;

import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.NETWORK.BL.GetPlanClass;
import co.com.rapxy.rapxy.MODAL.ListObjetc;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.RolObject;
import co.com.rapxy.rapxy.MODAL.StateObjetc;
import co.com.rapxy.rapxy.NETWORK.BL.GetHotelsClass;
import co.com.rapxy.rapxy.NETWORK.BL.GetListClass;
import co.com.rapxy.rapxy.NETWORK.BL.GetRolClass;
import co.com.rapxy.rapxy.NETWORK.BL.GetStateClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.*;


/**
 * Created by pedrodaza on 22/01/18.
 */

public class CallStorageResourceClass {

    public interface onGetCallAllStorageRecurse
    {
        void onGetStoragesave();
    }

    public CallStorageResourceClass(final Context context, final onGetCallAllStorageRecurse listenr)
    {

        //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new GetListClass().GetListForKey(FIREURL_LIST_IDTYPE, new GetListClass.onGetList() {
            @Override
            public void onGetList(List<ListObjetc> listObjetca) {
                LISTIDTYPE = listObjetca;
            }
        });
        new GetListClass().GetListForKey(FIREURL_LIST_TYPE_TRAVEL, new GetListClass.onGetList() {
            @Override
            public void onGetList(List<ListObjetc> listObjetca) {
                LISTTYPESTRAVEL = listObjetca;
            }
        });

        new GetStateClass().GetAllStates(new GetStateClass.onGetListState() {
            @Override
            public void onGetState(List<StateObjetc> stateObjetcs) {
                STATES = stateObjetcs;
            }
        });

        new GetRolClass().GetListRol(new GetRolClass.onGetRols() {
            @Override
            public void onGetRols(List<RolObject> rolObjectList) {
                ROLS = rolObjectList;
            }
        });

        new GetPlanClass().getPlans(new GetPlanClass.onGetPlans() {
            @Override
            public void onGetPlans(List<PlanObject> planObjects) {
                PLANS = planObjects;
            }
        });
        new GetHotelsClass().getHotels(new GetHotelsClass.onGetHotels() {
            @Override
            public void onGetHotels(List<AddresObject> hotelsObject) {
                //new AlertsClass().hideProggress(progressDialog);
                HOTELS = hotelsObject;
                listenr.onGetStoragesave();

            }
        });
        /*new GetPlanClass().GetPlanAll(new GetPlanClass.onGetPlans() {
            @Override
            public void onGetPlans(List<PlanObject> planObjects) {
                PLANS = planObjects;
            }
        });

        new GetRolClass().GetListRol(new GetRolClass.onGetRols() {
            @Override
            public void onGetRols(List<RolObject> rolObjectList) {
                ROLS = rolObjectList;
            }
        });

        new GetStateClass().GetAllStates(new GetStateClass.onGetListState() {
            @Override
            public void onGetState(List<StateObjetc> stateObjetcs) {
                STATES = stateObjetcs;
            }
        });


        new GetListClass().GetListForKey(FIREURL_LIST_IDTYPE, new GetListClass.onGetList() {
            @Override
            public void onGetList(List<ListObjetc> listObjetca) {
                LISTIDTYPE = listObjetca;
            }
        });

        new GetListClass().GetListForKey(FIREURL_LIST_PHYSIC_GOALS, new GetListClass.onGetList() {
            @Override
            public void onGetList(List<ListObjetc> listObjetca) {
                LISTMETAS = listObjetca;
            }
        });

        new GetPrecioClass().getPrecios(new GetPrecioClass.onGetPrecio() {
            @Override
            public void onGetPrecio(PrecioObject precioObject) {
                PRECIOS = precioObject;
            }
        });

        new GetListClass().GetListForKey(FIREURL_LIST_BLOOD_TYPE, new GetListClass.onGetList() {
            @Override
            public void onGetList(List<ListObjetc> listObjetca) {
                LISTBLOODTYPE = listObjetca;*/

                listenr.onGetStoragesave();
            /*}
        });*/


    }



}
