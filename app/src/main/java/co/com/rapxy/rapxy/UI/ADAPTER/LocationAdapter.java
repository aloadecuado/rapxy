package co.com.rapxy.rapxy.UI.ADAPTER;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;

import java.util.List;

import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Pedro on 05/02/2018.
 */

public class LocationAdapter extends ArrayAdapter<AddresObject> {

    Context context;
    List<AddresObject> audioRecordObjects;

    public LocationAdapter(@NonNull Context mcontext, @LayoutRes int resource, List<AddresObject> objects)
    {
        super(mcontext,resource, objects);

        context = mcontext;

    }




    @Override
    public View getView(int groupPosition, View convertView, ViewGroup parent) {

        Context con = null;
        View view = convertView;
        if (view == null)
        {
            //  LayoutInflater vi = (LayoutInflater)con.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LayoutInflater vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.adapter_location, null);
        }

        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        TextView tvAddres = (TextView) view.findViewById(R.id.tvAddres);



        AddresObject addresObject = getItem(groupPosition);

        tvName.setText("Nombre: " + addresObject.getName());
        tvAddres.setText("Direccion: " + addresObject.getAddres());


        return view;
    }
}
