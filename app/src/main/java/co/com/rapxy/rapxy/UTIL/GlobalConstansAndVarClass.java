package co.com.rapxy.rapxy.UTIL;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.ListObjetc;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.RolObject;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.StateObjetc;
import co.com.rapxy.rapxy.MODAL.UserObject;


/**
 * Created by Pedro on 30/12/2017.
 */

public class GlobalConstansAndVarClass {
    //firebase url
    public static String FIREURL_USERS = "Users";
    public static String FIREURL_LIST = "List";
    public static String FIREURL_STATE = "State";
    public static String FIREURL_PLAN = "Plan";
    public static String FIREURL_ROL = "Rol";
    public static String FIREURL_PRECIO = "Precio";
    public static String FIREURL_HOTEL= "Hotels";
    public static String FIREURL_SERVICE = "Services";
    public static String FIREURL_HOTEL_ADRESS= "Hotels/adress";
    public static String FIREURL_LIST_IDTYPE = "IdType";
    public static String FIREURL_LIST_BLOOD_TYPE = "BloodType";
    public static String FIREURL_LIST_PHYSIC_GOALS = "PhysicGoals";
    public static String FIREURL_LIST_TYPE_TRAVEL = "TipoTravel";



    //KeyS firebase
    public static String FIREKEY_USER = "";
    public static String FIREKEY_USERUID = "";

    // Object

    public static UserObject USEROBJECT;
    public static ServicesObject SERVICEOBJECT;
    public static UserObject USEROBJECTSERVICE;

    //List
    public static List<ListObjetc> LISTIDTYPE = null;
    public static List<ListObjetc> LISTBLOODTYPE = null;
    public static List<ListObjetc> LISTMETAS = null;
    public static List<StateObjetc> STATES = null;
    public static List<AddresObject> HOTELS = null;
    public static List<ListObjetc> LISTTYPESTRAVEL = null;
    public static List<PlanObject> PLANS = null;
    public static List<RolObject> ROLS = null;



    public static String PATH = "";


    public String[] GetArrayString(List<ListObjetc> listObjetcs)
    {
        String[] Strings = new String[listObjetcs.size()];
        int i = 0;
        for(ListObjetc listObjetc :listObjetcs)
        {
            Strings[i] = listObjetc.getDescription();
            i++;
        }

        return Strings;
    }

    public ListObjetc GetListObject(String Description, List<ListObjetc> listObjetcs)
    {
        ListObjetc listObjetc = new ListObjetc();
        for (ListObjetc listObjetc1 : listObjetcs)
        {
            if (listObjetc1.getDescription().equals(Description))
            {
                listObjetc = listObjetc1;
                break;
            }
        }

        return listObjetc;
    }

    public int getIndexList(String Description, List<ListObjetc> listObjetcs)
    {
        int index = 0;
        int i = 0;
        for (ListObjetc listObjetc1 : listObjetcs)
        {
            if (listObjetc1.equals(Description))
            {
                index = i;
                break;
            }
            i++;
        }

        return index;
    }

    public Bitmap loadBitmap(String url)
    {
        Bitmap bm = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try
        {
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            bm = BitmapFactory.decodeStream(bis);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (bis != null)
            {
                try
                {
                    bis.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            if (is != null)
            {
                try
                {
                    is.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return bm;
    }

}
