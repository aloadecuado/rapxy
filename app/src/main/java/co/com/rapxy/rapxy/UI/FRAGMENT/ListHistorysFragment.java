package co.com.rapxy.rapxy.UI.FRAGMENT;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.GetServicesClass;
import co.com.rapxy.rapxy.NETWORK.BL.GetUserClass;
import co.com.rapxy.rapxy.NETWORK.BL.SetServiceClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ACTIVITY.MenuActivity;
import co.com.rapxy.rapxy.UI.ADAPTER.ServicesAdapter;
import co.com.rapxy.rapxy.UI.DIALOG.PayTravelDialog;
import co.com.rapxy.rapxy.UI.DIALOG.QualifyDialog;
import co.com.rapxy.rapxy.UI.DIALOG.TravelVehiculoDialog;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.SERVICEOBJECT;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListHistorysFragment extends Fragment {

    @BindView(R.id.lvServices)
    ListView lvServices;

    List<ServicesObject> SERVICESOBJECTS;

    public ListHistorysFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_historys, container, false);

        ButterKnife.bind(this, view);
        setAllServices();

        return view;
    }

    private void setAllServices(){
        new GetServicesClass().getAllSercicesFull(getContext(), USEROBJECT, new GetServicesClass.onGetServicesUser() {
            @Override
            public void onGetServiceUser(List<ServicesObject> servicesObject) {
                setAdapterServices(servicesObject);
            }

            @Override
            public void onNoService() {

            }
        });
    }

    private void setAdapterServices(List<ServicesObject> servicesObjects){

        ServicesAdapter servicesAdapter = new ServicesAdapter(getContext(), R.layout.adapter_services, servicesObjects);
        lvServices.setAdapter(servicesAdapter);

        lvServices.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    final int position, long id) {


            }

        });
    }

}
