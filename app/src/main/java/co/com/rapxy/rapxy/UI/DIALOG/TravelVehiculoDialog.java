package co.com.rapxy.rapxy.UI.DIALOG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.GetServicesClass;
import co.com.rapxy.rapxy.NETWORK.BL.SetServiceClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ACTIVITY.MainActivity;
import co.com.rapxy.rapxy.UI.ACTIVITY.MenuActivity;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.SERVICEOBJECT;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECTSERVICE;

/**
 * Created by pedroalonsodazab on 24/02/18.
 */
@SuppressLint("ValidFragment")
public class TravelVehiculoDialog extends DialogFragment implements OnMapReadyCallback {


    public interface onGetUserAddresVehiculo
    {

        void onGetUser(ServicesObject servicesObject);
        void onGetPayService(ServicesObject servicesObject);

    }

    private static final int REQUEST_RECORD_CAMARE_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    ServicesObject servicesObject;
    ServicesObject SERVICEOBJECT;
    onGetUserAddresVehiculo Listener;
    UserObject userObject;
    UserObject userObjectService;

    UserObject userObjectCurrent;
    @SuppressLint("ValidFragment")
    public TravelVehiculoDialog(ServicesObject servicesObject, UserObject userObjectCurrent, UserObject userObject, UserObject userObjectService, onGetUserAddresVehiculo Listener)
    {
        this.servicesObject = servicesObject;
        this.Listener = Listener;
        this.userObject = userObject;
        this.userObjectService = userObjectService;
        this.userObjectCurrent = userObjectCurrent;
    }

    @BindView(R.id.map)
    MapView mapView;
    GoogleMap map;

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etAdrees)
    EditText etAdrees;

    @BindView(R.id.etPlan)
    EditText etPlan;

    @BindView(R.id.etPrice)
    EditText etPrice;

    @BindView(R.id.btOk)
    Button btOk;



    @BindView(R.id.btCancelar)
    Button btCancelar;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_vehiculo_travel, null);
        ButterKnife.bind(this, view);

        ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_RECORD_CAMARE_PERMISSION);

        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(this);
        SERVICEOBJECT = servicesObject;
        if(userObjectCurrent.getRol().getValue().equals("RU")){
            etName.setText(userObjectService.getNick());
        }else if (userObjectCurrent.getRol().getValue().equals("RV")){
            etName.setText(userObject.getNick());
        }

        etAdrees.setText(servicesObject.getAddress().getAddres());
        if (servicesObject.getPlan() != null)
        {
            etPlan.setText(servicesObject.getPlan().getDescription());
            etPrice.setText("$" + servicesObject.getPlan().getPrice());
        }




        SetButtons();
        suscibeService();

        builder.setNegativeButton(getContext().getResources().getString(R.string.btn_close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                TravelVehiculoDialog.this.getDialog().cancel();
            }
        });

        builder.setView(view);
        return builder.create();
    }

    private void suscibeService()
    {

        new GetServicesClass().subscribeServiceForId(getContext(), SERVICEOBJECT, USEROBJECT, new GetServicesClass.onGetServiceUser() {
            @Override
            public void onGetServiceUser(ServicesObject servicesObject, UserObject userObject) {
                TravelVehiculoDialog.this.servicesObject = servicesObject;
                userObjectService = userObject;

                SetButtons();

            }

            @Override
            public void onNoService() {

            }
        });
    }

    private void SetButtons()
    {
        if (userObjectCurrent.getRol() != null)
        {
            if (userObjectCurrent.getRol().getValue().equals("RU"))
            {
                if(servicesObject.getState().getValue().equals("ES"))
                {
                    SetButtonsUserPedding();
                }else if (servicesObject.getState().getValue().equals("TS"))
                {
                    SetButtonsUserAceppt();
                }else if (servicesObject.getState().getValue().equals("PPS"))
                {
                    SetButtonsUserProcesingPay();
                }
                else if (servicesObject.getState().getValue().equals("VLS"))
                {
                    SetButtonsUserHeLlegado();
                }
                else if (servicesObject.getState().getValue().equals("FS") || servicesObject.getState().getValue().equals("NS"))
                {
                    SetButtonsUserFinalizado();
                }

            }
            else if (userObjectCurrent.getRol().getValue().equals("RV"))
            {
                if(servicesObject.getState().getValue().equals("ES"))
                {
                    SetButtonsVehiculoPedding();
                }else if (servicesObject.getState().getValue().equals("TS"))
                {
                    SetButtonsVehiculoAceppt();
                }else if (servicesObject.getState().getValue().equals("PPS"))
                {
                    SetButtonsVehiculoProcesingPay();
                }
                else if (servicesObject.getState().getValue().equals("VLS"))
                {
                    SetButtonsVehiculoHeLlegado();
                }
                else if (servicesObject.getState().getValue().equals("FS") || servicesObject.getState().getValue().equals("NS"))
                {
                    SetButtonsVehiculoFinalizado();
                }
            }
        }
    }

    public void SetButtonsVehiculoPedding()
    {
        btOk.setText(getContext().getResources().getString(R.string.btn_travel_ok));
        btOk.setVisibility(View.VISIBLE);


        btCancelar.setText(getContext().getResources().getString(R.string.btn_cancel));
        btCancelar.setVisibility(View.VISIBLE);
    }


    private void SetButtonsUserPedding()
    {
        btOk.setText(getContext().getResources().getString(R.string.btn_travel_ok));
        btOk.setVisibility(View.INVISIBLE);


        btCancelar.setText(getContext().getResources().getString(R.string.btn_cancel));
        btCancelar.setVisibility(View.VISIBLE);
    }

    public void SetButtonsVehiculoAceppt()
    {
        try {
            if (btOk != null) {
                if (getContext().getResources().getString(R.string.btn_pay) != null) {
                    btOk.setText(getContext().getResources().getString(R.string.btn_travel_ok));
                    btOk.setVisibility(View.INVISIBLE);


                    btCancelar.setText(getContext().getResources().getString(R.string.btn_cancel));
                    btCancelar.setVisibility(View.VISIBLE);
                }
            }
        }
        catch (Exception e)
        {

        }

    }

    public void SetButtonsUserAceppt()
    {
        btOk.setText(getContext().getResources().getString(R.string.btn_pay));
        btOk.setVisibility(View.VISIBLE);


        btCancelar.setText(getContext().getResources().getString(R.string.btn_cancel));
        btCancelar.setVisibility(View.VISIBLE);
    }

    public void SetButtonsUserProcesingPay()
    {
        try {
            if (btOk != null) {
                if (getContext().getResources().getString(R.string.btn_pay) != null) {
                    btOk.setText(getContext().getResources().getString(R.string.btn_pay));
                    btOk.setVisibility(View.INVISIBLE);


                    btCancelar.setText(getContext().getResources().getString(R.string.btn_cancel));
                    btCancelar.setVisibility(View.INVISIBLE);
                }
            }
        }
        catch (Exception e)
        {

        }
    }



    public void SetButtonsVehiculoProcesingPay()
    {

        try {
            btOk.setText(getContext().getResources().getString(R.string.btn_i_arrived));
            btOk.setVisibility(View.VISIBLE);



            btCancelar.setText(getContext().getResources().getString(R.string.btn_cancel));
            btCancelar.setVisibility(View.INVISIBLE);
        }
        catch (Exception e)
        {

        }
    }

    public void SetButtonsUserHeLlegado()
    {
        try {
        btOk.setText(getContext().getResources().getString(R.string.btn_finish));
        btOk.setVisibility(View.VISIBLE);



        btCancelar.setText(getContext().getResources().getString(R.string.btn_cancel));
        btCancelar.setVisibility(View.INVISIBLE);
        }
        catch (Exception e)
        {

        }
    }



    public void SetButtonsVehiculoHeLlegado()
    {

        try {
            btOk.setText(getContext().getResources().getString(R.string.btn_i_arrived));
            btOk.setVisibility(View.INVISIBLE);


            btCancelar.setText(getContext().getResources().getString(R.string.btn_cancel));
            btCancelar.setVisibility(View.INVISIBLE);
        }
        catch (Exception e)
        {

        }
    }

    public void SetButtonsUserFinalizado()
    {
        try
        {
            TravelVehiculoDialog.this.getDialog().cancel();
        }catch (Exception e)
        {

        }
    }



    public void SetButtonsVehiculoFinalizado()
    {
        try
        {
            TravelVehiculoDialog.this.getDialog().cancel();
        }catch (Exception e)
        {

        }
    }

    @OnClick(R.id.btOk)
    public void Aceotar()
    {
        if (btOk.getText().equals(getContext().getResources().getString(R.string.btn_travel_ok))) {
            new SetServiceClass().setServiceAceptForId(getContext(), servicesObject, new SetServiceClass.onGetService() {
                @Override
                public void onGetService(ServicesObject servicesObject) {
                    Listener.onGetUser(servicesObject);
                    try
                    {
                        TravelVehiculoDialog.this.getDialog().cancel();
                    }catch (Exception e)
                    {

                    }
                }
            });
        }else if (btOk.getText().equals(getContext().getResources().getString(R.string.btn_pay)))
        {
            TravelVehiculoDialog.this.getDialog().cancel();
            Listener.onGetPayService(servicesObject);
            try
            {
                TravelVehiculoDialog.this.getDialog().cancel();
            }catch (Exception e)
            {

            }

        }else if (btOk.getText().equals(getContext().getResources().getString(R.string.btn_i_arrived)))
        {
            new SetServiceClass().setServiceIArrivedForId(getContext(), servicesObject, new SetServiceClass.onGetService() {
                @Override
                public void onGetService(ServicesObject servicesObject) {
                    Listener.onGetUser(servicesObject);
                    try
                    {
                        TravelVehiculoDialog.this.getDialog().cancel();
                    }catch (Exception e)
                    {

                    }
                }
            });

        }else if (btOk.getText().equals(getContext().getResources().getString(R.string.btn_finish)))
        {
            new SetServiceClass().setServiceForId(getContext(), servicesObject, new SetServiceClass.onGetService() {
                @Override
                public void onGetService(ServicesObject servicesObject) {
                    Listener.onGetUser(servicesObject);
                    try
                    {
                        TravelVehiculoDialog.this.getDialog().cancel();
                    }catch (Exception e)
                    {

                    }
                }
            });

        }

    }



    @OnClick(R.id.btCancelar)
    public void Cancelar()
    {
        new SetServiceClass().setServiceCancelForId(getContext(), servicesObject, new SetServiceClass.onGetService() {
            @Override
            public void onGetService(ServicesObject servicesObject) {
                Listener.onGetUser(servicesObject);
                try
                {
                    TravelVehiculoDialog.this.getDialog().cancel();
                }catch (Exception e)
                {

                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;



        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {

            if (servicesObject.getAddress() != null) {

                SERVICEOBJECT = servicesObject;
                setLocation(new LatLng(servicesObject.getAddress().getLat(), servicesObject.getAddress().getLog()));
            }


        }
    }

    private void setLocation(LatLng latLng)
    {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {

            RemoveAllMarkets();
            addMarket(new LatLng(latLng.latitude, latLng.longitude), "Mi Destino");
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }

    private void RemoveAllMarkets()
    {
        for (Marker marker : markerOptionss)
        {
            marker.remove();
        }

        markerOptionss = new ArrayList<>();
    }

    List<Marker> markerOptionss = new ArrayList<>();
    private Marker addMarket(LatLng latLng, String title)
    {
        MarkerOptions markerOptions = new MarkerOptions();
        Marker marker = map.addMarker(markerOptions
                .position(latLng)
                .title(title));

        markerOptionss.add(marker);

        return marker;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_CAMARE_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (servicesObject.getAddress() != null) {

                    SERVICEOBJECT = servicesObject;
                    setLocation(new LatLng(servicesObject.getAddress().getLat(), servicesObject.getAddress().getLog()));
                }
                break;
        }
        if (!permissionToRecordAccepted) getActivity().finish();

    }
}
