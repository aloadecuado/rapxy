package co.com.rapxy.rapxy.UI.ADAPTER;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.R;

/**
 * Created by Pedro on 06/02/2018.
 */

public class VehiculoAdapter extends RecyclerView.Adapter<VehiculoAdapter.ViewHolder>   {

    private List<UserObject> userObjects;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView imVehiculo;
        public TextView tvName;
        public TextView tvAdress;

        public ImageView btFavorite;
        public ImageView btimages;
        public ImageView btService;

        public ViewHolder(View v) {
            super(v);


            imVehiculo = v.findViewById(R.id.imVehiculo);

            tvName = v.findViewById(R.id.tvName);
            tvAdress = v.findViewById(R.id.tvAdress);

            btFavorite = v.findViewById(R.id.btFavorite);
            btimages = v.findViewById(R.id.btimages);
            btService = v.findViewById(R.id.btService);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    private Context context;
    onGetUser listener;
    private UserObject CurrenUser;
    public VehiculoAdapter(UserObject CurrenUser, List<UserObject> userObjects, Context context, onGetUser listener) {
        this.userObjects = userObjects;
        this.context = context;
        this.listener = listener;
        this.CurrenUser = CurrenUser;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public VehiculoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_vehiculo, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public interface onGetUser
    {
        void onGetUser(UserObject userObject);
        void onGetListImage(UserObject userObject);
        void onGetUserFavorite(UserObject userObject, boolean favorite);
    }
    int position = 0;
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        this.position = position;

        if (userObjects.get(position).getUrlsPhotos() != null)
        {
            Glide.with(context).load(userObjects.get(position).getUrlsPhotos().get(0)).into(holder.imVehiculo);
        }

        holder.tvName.setText(userObjects.get(position).getNick());
        holder.tvAdress.setText(userObjects.get(position).getDescription());

        if (CurrenUser.getFavorites() != null)
        {
            Boolean bExite = false;
            for (String sFavorite : CurrenUser.getFavorites())
            {
                if (sFavorite.equals(userObjects.get(position).getId()))
                {
                    bExite = true;
                }
            }

            if(bExite)
            {
                holder.btFavorite.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
            }
            else
            {
                holder.btFavorite.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
            }
        }


        holder.btFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onGetUserFavorite(userObjects.get(position), false);

            }
        });

        holder.btimages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onGetListImage(userObjects.get(position));
            }
        });

        holder.btService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onGetUser(userObjects.get(position));
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return userObjects.size();
    }
}
