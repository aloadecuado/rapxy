package co.com.rapxy.rapxy.UI.ADAPTER;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;

import java.util.List;

import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.GetUserClass;
import co.com.rapxy.rapxy.R;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;

/**
 * Created by Pedro on 05/02/2018.
 */

public class ServicesAdapter extends ArrayAdapter<ServicesObject> {


    Context context;
    List<ServicesObject> audioRecordObjects;


    public ServicesAdapter(@NonNull Context mcontext, @LayoutRes int resource, List<ServicesObject> objects)
    {
        super(mcontext,resource, objects);

        context = mcontext;

    }




    @Override
    public View getView(int groupPosition, View convertView, ViewGroup parent) {

        Context con = null;
        View view = convertView;
        if (view == null)
        {
            //  LayoutInflater vi = (LayoutInflater)con.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LayoutInflater vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.adapter_services, null);
        }

        ImageView imPhoto = (ImageView) view.findViewById(R.id.imPhoto);

        TextView tvNameOther = (TextView) view.findViewById(R.id.tvNameOther);
        TextView tvAddres = (TextView) view.findViewById(R.id.tvAddres);
        TextView tvPlan = (TextView) view.findViewById(R.id.tvPlan);
        TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);

        ServicesObject servicesObject = getItem(groupPosition);
        String isUser = "";

        if (USEROBJECT.getRol() != null){
            if(USEROBJECT.getRol().getValue().equals("RU")){

                isUser = servicesObject.getIdDriver();

            }else if (USEROBJECT.getRol().getValue().equals("RV")){
                isUser = servicesObject.getIdUser();
            }
        }


        new GetUserClass().getUserForId(context, isUser, new GetUserClass.onGetUser() {
            @Override
            public void onGetUser(UserObject userObjects) {
                tvNameOther.setText("Nombre: " + userObjects.getNick());
                tvAddres.setText("Direccion: " + servicesObject.getAddress().getAddres());
                tvPlan.setText("Plan: " + servicesObject.getPlan().getDescription());
                tvPrice.setText("Precio: $" + servicesObject.getPlan().getPrice());

                if (userObjects.getUrlsPhotos() != null){
                    if(userObjects.getUrlsPhotos().size() >= 1){
                        Glide.with(context).load(userObjects.getUrlsPhotos().get(0)).into(imPhoto);
                    }
                }



            }
        });




        return view;
    }
}
