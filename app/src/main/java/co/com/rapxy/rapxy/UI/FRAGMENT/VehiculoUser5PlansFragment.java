package co.com.rapxy.rapxy.UI.FRAGMENT;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ADAPTER.PlanAdapter;
import co.com.rapxy.rapxy.UI.DIALOG.AddPlanDialog;
import co.com.rapxy.rapxy.UI.DIALOG.DetailPlanDialog;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehiculoUser5PlansFragment extends Fragment {


    public VehiculoUser5PlansFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.lvPlans)
    ListView lvPlans;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_vehiculo_user5_plans, container, false);

        ButterKnife.bind(this, view);
        SetAdapterList();

        // Inflate the layout for this fragment
        return view;
    }

    private void SetAdapterList()
    {

        if (USEROBJECT.getPlan() != null)
        {
            PlanAdapter planAdapter = new PlanAdapter(getActivity(), R.layout.adapter_plan, USEROBJECT.getPlan());
            lvPlans.setAdapter(planAdapter);
            lvPlans.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    DetailPlanDialog detailPlanDialog = new DetailPlanDialog(getContext(), USEROBJECT, USEROBJECT.getPlan().get(position), new DetailPlanDialog.onGetUser() {
                        @Override
                        public void onGetUser(UserObject userObject) {
                            USEROBJECT = userObject;
                            SetAdapterList();

                        }
                    });

                    detailPlanDialog.show(fm, "fragment_edit_name");
                }
            });
        }

    }
    @OnClick(R.id.btAdd)
    public void Add()
    {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        AddPlanDialog addPlanDialog = new AddPlanDialog(getActivity(), USEROBJECT, new AddPlanDialog.onGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {

                USEROBJECT = userObject;
                SetAdapterList();
            }
        });


        addPlanDialog.show(fm, "fragment_edit_name");

    }

}
