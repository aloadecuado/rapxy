package co.com.rapxy.rapxy.UTIL;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import co.com.rapxy.rapxy.R;

/**
 * Created by pedrodaza on 31/01/18.
 */

public class WriteReadStorageClass {

    public void setDataString(Activity context, String string, String key)
    {
        SharedPreferences sharedPref = context.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, string);
        editor.commit();


    }

    public String getDataString(Activity activity, String key)
    {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);

        String App = sharedPref.getString(key, "Sin asignar");

        return App;
    }
}
