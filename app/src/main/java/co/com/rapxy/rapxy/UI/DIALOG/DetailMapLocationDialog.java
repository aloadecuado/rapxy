package co.com.rapxy.rapxy.UI.DIALOG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

/**
 * Created by Pedro on 05/02/2018.
 */
@SuppressLint("ValidFragment")
public class DetailMapLocationDialog extends DialogFragment implements OnMapReadyCallback {

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;



        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {

            setLocation(new LatLng(ADDRESOBJEC.getLat(), ADDRESOBJEC.getLog()));
            RemoveAllMarkets();
            addMarket(new LatLng(ADDRESOBJEC.getLat(), ADDRESOBJEC.getLog()), ADDRESOBJEC.getName());


        }
    }

    private void setLocation(LatLng latLng)
    {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {

            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }
    public interface onGetUser
    {
        void onGetUser(UserObject userObject);
    }

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etAdrees)
    EditText etAdrees;

    @BindView(R.id.etTowel)
    EditText etTowel;

    @BindView(R.id.etFood)
    EditText etFood;

    AddresObject ADDRESOBJEC;
    UserObject USEROBJECT;
    int position;

    @BindView(R.id.map)
    MapView mapView;
    GoogleMap map;

    onGetUser listener;
    @SuppressLint("ValidFragment")
    public DetailMapLocationDialog(AddresObject addresObject, UserObject userObject, int position, onGetUser onGetUser)
    {
        this.ADDRESOBJEC = addresObject;
        this.USEROBJECT = userObject;
        listener = onGetUser;
        this.position = position;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_detail_location, null);
        ButterKnife.bind(this, view);

        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(this);


        etName.setText(ADDRESOBJEC.getName());
        etAdrees.setText(ADDRESOBJEC.getAddres());
        etAdrees.setFocusable(false);
        etAdrees.setEnabled(false);

        etTowel.setText(ADDRESOBJEC.getTowell());
        etFood.setText(ADDRESOBJEC.getFoot());

        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();

    }

    List<Marker> markerOptionss = new ArrayList<>();
    private Marker addMarket(LatLng latLng, String title)
    {
        MarkerOptions markerOptions = new MarkerOptions();
        Marker marker = map.addMarker(markerOptions
                .position(latLng)
                .title(title));

        markerOptionss.add(marker);

        return marker;
    }

    private void RemoveAllMarkets()
    {
        for (Marker marker : markerOptionss)
        {
            marker.remove();
        }

        markerOptionss = new ArrayList<>();
    }

    @OnClick(R.id.btOk)
    public void Ok()
    {
        DetailMapLocationDialog.this.getDialog().cancel();
    }

    @OnClick(R.id.btErase)
    public void Erase()
    {
        new AlertsClass().showAlerInfoYesOrNo(getContext(), "Información", "¿Desea Eliminar esta localización?", new AlertsClass.onGetData() {
            @Override
            public void onGetDataOk() {

                USEROBJECT.getAddres().remove(position);

                new SetUserClass().SetUserForid(USEROBJECT, new SetUserClass.onGetUser() {
                    @Override
                    public void onGetUser(UserObject userObject) {
                        listener.onGetUser(userObject);
                        DetailMapLocationDialog.this.getDialog().cancel();
                    }
                });

            }

            @Override
            public void onGetDataCalcel() {

            }
        });



    }

    @OnClick(R.id.btSave)
    public void Save()
    {
        ADDRESOBJEC.setName(etName.getText().toString());
        ADDRESOBJEC.setAddres(etAdrees.getText().toString());
        ADDRESOBJEC.setTowell(etTowel.getText().toString());
        ADDRESOBJEC.setFoot(etFood.getText().toString());
        USEROBJECT.getAddres().remove(position);
        USEROBJECT.getAddres().add(position, ADDRESOBJEC);

        new SetUserClass().SetUserForid(USEROBJECT, new SetUserClass.onGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {
                listener.onGetUser(userObject);
                DetailMapLocationDialog.this.getDialog().cancel();
            }
        });
    }

}
