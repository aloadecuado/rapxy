package co.com.rapxy.rapxy.UI.ADAPTER;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import java.util.List;

import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.R;

/**
 * Created by pedrodaza on 8/02/18.
 */

public class PlanAdapter  extends ArrayAdapter<PlanObject> {

    Context context;
    List<PlanObject> planObjects;

    public PlanAdapter(@NonNull Context mcontext, @LayoutRes int resource, List<PlanObject> planObjects)
    {
        super(mcontext,resource, planObjects);

        context = mcontext;

    }




    @Override
    public View getView(int groupPosition, View convertView, ViewGroup parent) {

        Context con = null;
        View view = convertView;
        if (view == null)
        {
            //  LayoutInflater vi = (LayoutInflater)con.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LayoutInflater vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.adapter_plan, null);
        }

        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        TextView tvDescrition = (TextView) view.findViewById(R.id.tvDescrition);
        TextView tvPrice = (TextView) view.findViewById(R.id.tvPrice);



        PlanObject planObject = getItem(groupPosition);

        tvName.setText("Nombre: " + planObject.getNombre());
        tvDescrition.setText("Descripcion: " + planObject.getDescription());
        tvPrice.setText("Price: "+ planObject.getFormatPrice());
;


        return view;
    }
}
