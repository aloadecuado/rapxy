package co.com.rapxy.rapxy.NETWORK.BL;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.WS.GetObjectClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.FIREURL_USERS;


/**
 * Created by Pedro on 25/01/2018.
 */

public class GetUserClass {

    public interface onGetUsers
    {
        void onGetListUsers(List<UserObject> userObjects);
    }
    public interface onGetUser
    {
        void onGetUser(UserObject userObjects);
    }


    public void getListUsersFilterName(Context ctx, String Name, final onGetUsers listener)
    {
        new GetObjectClass().getListenerListObjectUidKey(FIREURL_USERS, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                ArrayList<UserObject> userObjects = new ArrayList<>();

                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    UserObject userObject = snapshot1.getValue(UserObject.class);
                    userObject.setId(snapshot1.getKey());

                    userObjects.add(userObject);
                }

                if(userObjects.size() >= 1)
                {
                    listener.onGetListUsers(userObjects);
                }
            }

            @Override
            public void onGetNullObject() {

            }

            @Override
            public void OnGetError(String Err) {

            }
        });
    }


    public void getListListenerUsersForStateActivo(Context ctx, final UserObject userObject, final onGetUsers listener)
    {

        new GetObjectClass().getListenerListObjectUidKey(FIREURL_USERS, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                ArrayList<UserObject> userObjects = new ArrayList<>();
                ArrayList<UserObject> userObjectsFavorite = new ArrayList<>();
                ArrayList<UserObject> userObjectsNoFavorite = new ArrayList<>();

                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    UserObject userObjectREsponse = snapshot1.getValue(UserObject.class);
                    userObjectREsponse.setId(snapshot1.getKey());

                    if (userObjectREsponse.getState() != null)
                    {
                        if (userObjectREsponse.getState().getValue().equals("AS"))
                        {
                            if (userObject.getFavorites() != null)
                            {
                                for (String sFavorite : userObject.getFavorites())
                                {
                                    if (sFavorite.equals(userObjectREsponse.getId()))
                                    {
                                        userObjectsFavorite.add(userObjectREsponse);
                                        break;
                                    }
                                    else
                                    {

                                    }
                                }


                            }




                        }


                    }

                }

                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    UserObject userObjectREsponse = snapshot1.getValue(UserObject.class);
                    userObjectREsponse.setId(snapshot1.getKey());

                    if (userObjectREsponse.getState() != null)
                    {
                        if (userObjectREsponse.getState().getValue().equals("AS"))
                        {
                            if (userObject.getFavorites() != null)
                            {
                                Boolean isFavorite = false;
                                for (String sFavorite : userObject.getFavorites())
                                {

                                    if (sFavorite.equals(userObjectREsponse.getId()))
                                    {
                                        isFavorite = true;
                                        break;
                                    }
                                }
                                if(!isFavorite)
                                {
                                    userObjectsNoFavorite.add(userObjectREsponse);
                                }


                            }
                            else
                            {
                                userObjectsNoFavorite.add(userObjectREsponse);
                            }




                        }

                    }

                }

                userObjects.addAll(userObjectsFavorite);
                userObjects.addAll(userObjectsNoFavorite);
                if(userObjects.size() >= 1)
                {
                    listener.onGetListUsers(userObjects);
                }
                else
                {
                    listener.onGetListUsers(new ArrayList<UserObject>());
                }
            }

            @Override
            public void onGetNullObject() {

            }

            @Override
            public void OnGetError(String Err) {

            }
        });
    }

    public void getUserForId(final Context context, String userId, final onGetUser listener)
    {
        new GetObjectClass().getSimpleObjectsForKey(FIREURL_USERS, userId, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                    UserObject userObject = snapshot.getValue(UserObject.class);

                    if(userObject != null)
                    {
                        listener.onGetUser(userObject);
                    }
                    else
                    {
                        new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_user));
                    }
            }

            @Override
            public void onGetNullObject() {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_user));
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
            }
        });

    }
}
