package co.com.rapxy.rapxy.UI.DIALOG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TabHost;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTabHost;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.FRAGMENT.VehiculoUser1DataBasicFragment;
import co.com.rapxy.rapxy.UI.FRAGMENT.VehiculoUser2DocumentsFragment;
import co.com.rapxy.rapxy.UTIL.AlertsClass;
import co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.LISTIDTYPE;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.PLANS;

/**
 * Created by pedrodaza on 7/02/18.
 */
@SuppressLint("ValidFragment")
public class DetailTravelDialog extends DialogFragment implements OnMapReadyCallback {

    private FragmentTabHost mTabHost;

    public interface onGetUserAddresVehiculo
    {

        void onGetUser(AddresObject addresObject, UserObject userObject, UserObject userObjectVehiculo, PlanObject planObject);

    }


    List<AddresObject> Hotels;
    UserObject userObject;
    UserObject userObjectDrive;
    List<AddresObject> addresObjects;
    AddresObject ADDREESSSSELECT;
    PlanObject PLANOBJECT;
    onGetUserAddresVehiculo listener;
    @SuppressLint("ValidFragment")
    public DetailTravelDialog(List<AddresObject> Hotels, UserObject userObject, UserObject userObjectDrive, onGetUserAddresVehiculo onGetUser)
    {
        this.Hotels = Hotels;
        this.userObject = userObject;
        this.userObjectDrive = userObjectDrive;
        listener = onGetUser;
    }

    @BindView(R.id.map)
    MapView mapView;
    GoogleMap map;

    @BindView(R.id.tabHost)
    TabHost tabHost;

    @BindView(R.id.spLocation)
    Spinner spLocation;

    @BindView(R.id.spPlan)
    Spinner spPlan;

    private static final int REQUEST_RECORD_CAMARE_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_detail_travel, null);
        ButterKnife.bind(this, view);

        ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_RECORD_CAMARE_PERMISSION);
        tabHost.setup();


        TabHost.TabSpec tabpage2 = tabHost.newTabSpec("two");
        tabpage2.setContent(R.id.tab2);
        tabpage2.setIndicator("", getResources().getDrawable(R.drawable.ic_person_black_24dp));

        TabHost.TabSpec tabpage3 = tabHost.newTabSpec("tree");
        tabpage3.setContent(R.id.tab3);
        tabpage3.setIndicator("", getResources().getDrawable(R.drawable.ic_drive_eta_black_24dp));

        tabHost.addTab(tabpage2);
        tabHost.addTab(tabpage3);

        String[] asTipoIds = getArrayStringAdress(userObject.getAddres());
        String[] asPlans = getArrayStringPlans(userObjectDrive.getPlan());

        spLocation.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, asTipoIds));
        spPlan.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, asPlans));

        if (userObject.getAddres() != null && userObject.getAddres().size() >= 1) {
            addresObjects = userObject.getAddres();

        }
        if (userObjectDrive.getPlan() != null && userObjectDrive.getPlan().size() >= 1)
        {
            PLANOBJECT = userObjectDrive.getPlan().get(0);
        }
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {

                if (tabId.equals("two") )
                {
                    String[] asTipoIds = getArrayStringAdress(userObject.getAddres());
                    if (userObject.getAddres() != null && userObject.getAddres().size() >= 1) {
                        addresObjects = userObject.getAddres();
                    }

                    spLocation.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, asTipoIds));
                }
                else if (tabId.equals("tree") )
                {
                    String[] asTipoIds = getArrayStringAdress(userObjectDrive.getAddres());
                    if (userObjectDrive.getAddres() != null && userObjectDrive.getAddres().size() >= 1) {
                        addresObjects = userObjectDrive.getAddres();
                    }

                    spLocation.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, asTipoIds));
                }

            }
        });
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(this);


        spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ADDREESSSSELECT = addresObjects.get(position);

                setLocation(new LatLng(ADDREESSSSELECT.getLat(), ADDREESSSSELECT.getLog()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (userObjectDrive.getPlan() != null) {
            spPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PLANOBJECT = userObjectDrive.getPlan().get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }else {


            new AlertsClass().showAlerInfoYesOrNo(getContext(), getString(R.string.alert_title_information), getString(R.string.vehiculo_no_plan), new AlertsClass.onGetData() {
                @Override
                public void onGetDataOk() {

                }

                @Override
                public void onGetDataCalcel() {

                }
            });
            DetailTravelDialog.this.getDialog().cancel();
        }
        builder.setView(view)
                .setPositiveButton(R.string.btn_Ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {


                listener.onGetUser(ADDREESSSSELECT, userObject, userObjectDrive, PLANOBJECT);




            }
        })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DetailTravelDialog.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }
    private String[] getArrayStringAdress(List<AddresObject> addresObjects)
    {
        if (addresObjects != null) {
            String[] sAdreess = new String[addresObjects.size()];
            int i = 0;
            for (AddresObject addresObject : addresObjects) {
                sAdreess[i] = addresObject.getName();
                i++;
            }
            return sAdreess;
        }
        else
        {
            String[] sAdreess = {""};
            return sAdreess;
        }



    }

    private String[] getArrayStringPlans(List<PlanObject> planObjects)
    {
        if (planObjects != null) {
            String[] splans = new String[planObjects.size()];
            int i = 0;
            for (PlanObject planObject : planObjects) {
                splans[i] = "Plan: " + planObject.getDescription() + "\nPrecio: " + planObject.getPrice();
                i++;
            }
            return splans;
        }
        else
        {
            String[] splans = {""};
            return splans;
        }



    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;



        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {

            if (userObject.getAddres() != null && userObject.getAddres().size() >= 1) {
                ADDREESSSSELECT = userObject.getAddres().get(0);
                setLocation(new LatLng(userObject.getAddres().get(0).getLat(), userObject.getAddres().get(0).getLog()));
            }


        }
    }

    private void setLocation(LatLng latLng)
    {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {

            RemoveAllMarkets();
            addMarket(new LatLng(latLng.latitude, latLng.longitude), "Mi Destino");
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }

    private void RemoveAllMarkets()
    {
        for (Marker marker : markerOptionss)
        {
            marker.remove();
        }

        markerOptionss = new ArrayList<>();
    }

    List<Marker> markerOptionss = new ArrayList<>();
    private Marker addMarket(LatLng latLng, String title)
    {
        MarkerOptions markerOptions = new MarkerOptions();
        Marker marker = map.addMarker(markerOptions
                .position(latLng)
                .title(title));

        markerOptionss.add(marker);

        return marker;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_CAMARE_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (userObject.getAddres() != null && userObject.getAddres().size() >= 1) {
                    setLocation(new LatLng(userObject.getAddres().get(0).getLat(), userObject.getAddres().get(0).getLog()));
                }
                break;
        }
        if (!permissionToRecordAccepted) getActivity().finish();

    }
}
