package co.com.rapxy.rapxy.NETWORK.WS;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Pedro on 30/12/2017.
 */

public class SetObjectClass {

    public interface onGetObject
    {
        void onGetObject(DataSnapshot snapshot);
        void onGetError(String Err);
    }

    FirebaseDatabase DataBase;
    public  SetObjectClass()
    {
        DataBase = FirebaseDatabase.getInstance();
    }

    public void SetObjectKeyForAutoKey(final Object object, String Child, final onGetObject listener)
    {
        final DatabaseReference myRef = DataBase.getReference(Child);
        myRef.push().setValue(object, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    listener.onGetError( databaseError.getMessage());
                    System.out.println("Data could not be saved " + databaseError.getMessage());
                } else {
                    DatabaseReference myRefKey = myRef.child(databaseReference.getKey());

                    myRefKey.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            listener.onGetObject(dataSnapshot);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            listener.onGetError(databaseError.getMessage());
                        }
                    });

                    System.out.println("Data saved successfully.");
                }
            }
        });
    }

    public void SetObjectKeyForCustomKey(final Object object, String Child, String Key, final onGetObject listener)
    {
        final DatabaseReference myRef = DataBase.getReference(Child).child(Key);
        myRef.setValue(object, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    listener.onGetError( databaseError.getMessage());
                    System.out.println("Data could not be saved " + databaseError.getMessage());
                } else {


                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            listener.onGetObject(dataSnapshot);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            listener.onGetError(databaseError.getMessage());
                        }
                    });

                    System.out.println("Data saved successfully.");
                }
            }
        });
    }



}
