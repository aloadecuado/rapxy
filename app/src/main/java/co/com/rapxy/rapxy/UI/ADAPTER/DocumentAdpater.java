package co.com.rapxy.rapxy.UI.ADAPTER;

import android.content.Context;
import android.database.Observable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import co.com.rapxy.rapxy.R;

/**
 * Created by pedrodaza on 31/01/18.
 */

public class DocumentAdpater extends RecyclerView.Adapter<DocumentAdpater.ViewHolder>  {

    private String[] mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView imDocument;

        public ViewHolder(View v) {
            super(v);

            imDocument = v.findViewById(R.id.imDocument);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    private Context context;
    onGetClick listener;
    public DocumentAdpater(String[] myDataset, Context context, onGetClick listener) {
        mDataset = myDataset;
        this.context = context;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DocumentAdpater.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_image_document, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public interface onGetClick
    {
        void onGetUrlImage(String urlImage);
    }
    int position = 0;
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        this.position = position;
        Glide.with(context).load(mDataset[position]).into(holder.imDocument);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onGetUrlImage(mDataset[position]);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
