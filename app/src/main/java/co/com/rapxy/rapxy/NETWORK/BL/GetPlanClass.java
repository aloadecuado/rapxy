package co.com.rapxy.rapxy.NETWORK.BL;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.ProgressBar;

import com.firebase.ui.auth.data.model.User;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.GetUserClass;
import co.com.rapxy.rapxy.NETWORK.WS.GetObjectClass;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.*;

/**
 * Created by pedrodaza on 8/02/18.
 */

public class GetPlanClass {

    public interface onGetPlans
    {
        void onGetPlans(List<PlanObject> planObjects);
    }

    public void getPlans(final onGetPlans listener)
    {
        new GetObjectClass().getSimpleListObjectUidKey(FIREURL_PLAN, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                List<PlanObject> planObjects = new ArrayList<>();

                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    PlanObject planObject = snapshot1.getValue(PlanObject.class);
                    planObject.setId(snapshot1.getKey());
                    planObjects.add(planObject);
                }

                if (planObjects.size() >= 1)
                {
                    listener.onGetPlans(planObjects);

                }
            }

            @Override
            public void onGetNullObject() {

            }

            @Override
            public void OnGetError(String Err) {

            }
        });
    }

    public void getPlanForUserId(Context context, final UserObject userObject, final onGetPlans listener)
    {

        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        getPlans(new onGetPlans() {
            @Override
            public void onGetPlans(List<PlanObject> planObjects) {
                List<PlanObject> planObjects1 = new ArrayList<>();
                for (PlanObject planObject1 : planObjects)
                {
                    if(userObject.getPlan() != null)
                    {
                        boolean bExiste = false;
                        for (PlanObject planObject2 : userObject.getPlan())
                        {
                            if(planObject1.getId().equals(planObject2.getId()))
                            {
                                bExiste = true;
                                break;
                            }
                        }

                        if (!bExiste)
                        {
                            planObjects1.add(planObject1);
                        }
                    }
                    else
                    {
                        planObjects1 = planObjects;
                        break;
                    }

                }

                if(planObjects1.size() >= 1)
                {
                    listener.onGetPlans(planObjects1);
                    new AlertsClass().hideProggress(progressDialog);
                }
            }
        });

    }
}
