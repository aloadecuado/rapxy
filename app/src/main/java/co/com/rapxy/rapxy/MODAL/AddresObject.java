package co.com.rapxy.rapxy.MODAL;

/**
 * Created by pedrodaza on 30/01/18.
 */

public class AddresObject {

    private String name = "";
    private String addres = "";
    private double lat = 0.0;
    private double log = 0.0;
    private String towell = "";
    private String foot = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLog() {
        return log;
    }

    public void setLog(double log) {
        this.log = log;
    }

    public String getTowell() {
        return towell;
    }

    public void setTowell(String towell) {
        this.towell = towell;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }


}
