package co.com.rapxy.rapxy.MODAL;

import java.util.List;

/**
 * Created by Pedro on 29/01/2018.
 */

public class UserObject {

    private String id = "";

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    private String uid = "";
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public ListObjetc getIdType() {
        return idType;
    }

    public void setIdType(ListObjetc idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }



    public List<AddresObject> getAddres() {
        return addres;
    }

    public void setAddres(List<AddresObject> addres) {
        this.addres = addres;
    }

    private String nick = "";
    private String dateRegister = "";
    private ListObjetc idType = null;
    private String idNumber = "";

    public List<String> getUrlsDocuments() {
        return urlsDocuments;
    }

    public void setUrlsDocuments(List<String> urlsDocuments) {
        this.urlsDocuments = urlsDocuments;
    }

    private List<String> urlsDocuments = null;
    private List<AddresObject> addres = null;
    private String email = "";

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFaceUid() {
        return faceUid;
    }

    public void setFaceUid(String faceUid) {
        this.faceUid = faceUid;
    }

    private String faceUid = "";

    public String getEmailComprobed() {
        return emailComprobed;
    }

    public void setEmailComprobed(String emailComprobed) {
        this.emailComprobed = emailComprobed;
    }

    private String emailComprobed = "";

    public String getEmailUid() {
        return emailUid;
    }

    public void setEmailUid(String emailUid) {
        this.emailUid = emailUid;
    }

    private String emailUid = "";

    public List<PhotosObject> getPhotos() {
        return photos;
    }

    public void setPhotos(List<PhotosObject> photos) {
        this.photos = photos;
    }

    private List<PhotosObject> photos = null;

    public String getNumberCellphone() {
        return numberCellphone;
    }

    public void setNumberCellphone(String numberCellphone) {
        this.numberCellphone = numberCellphone;
    }

    private String numberCellphone = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description = "";

    public List<String> getUrlsPhotos() {
        return urlsPhotos;
    }

    public void setUrlsPhotos(List<String> urlsPhotos) {
        this.urlsPhotos = urlsPhotos;
    }

    private List<String> urlsPhotos = null;

    public StateObjetc getState() {
        return state;
    }

    public void setState(StateObjetc state) {
        this.state = state;
    }

    private StateObjetc state = null;

    public List<String> getFavorites() {
        return Favorites;
    }

    public void setFavorites(List<String> favorites) {
        Favorites = favorites;
    }

    private List<String> Favorites = null;

    public List<PlanObject> getPlan() {
        return plan;
    }

    public void setPlan(List<PlanObject> plan) {
        this.plan = plan;
    }

    private List<PlanObject> plan = null;

    public RolObject getRol() {
        return rol;
    }

    public void setRol(RolObject rol) {
        this.rol = rol;
    }

    private RolObject rol = null;

    public List<CreditCardObject> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(List<CreditCardObject> creditCards) {
        this.creditCards = creditCards;
    }

    private List<CreditCardObject> creditCards = null;



}
