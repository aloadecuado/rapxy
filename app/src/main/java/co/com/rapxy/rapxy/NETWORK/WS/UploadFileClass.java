package co.com.rapxy.rapxy.NETWORK.WS;

import android.net.Uri;


import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

/**
 * Created by pedrodaza on 24/01/18.
 */

public class UploadFileClass {

    public interface OnSetDataFile
    {
        void OnSetFile(String UrlDownload);
        void OnSetError(String error);
    }

    FirebaseStorage storage = FirebaseStorage.getInstance();
    public void UploadFile(String Referencia, String NombreArchivo, byte[] Archivo, final OnSetDataFile listener)
    {
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

        // Create a reference to "mountains.jpg"
        StorageReference mountainsRef = storageRef.child(NombreArchivo);

        // Create a reference to 'images/mountains.jpg'
        StorageReference mountainImagesRef = storageRef.child(Referencia);

        mountainsRef.getName().equals(mountainImagesRef.getName());    // true
        mountainsRef.getPath().equals(mountainImagesRef.getPath());

        UploadTask uploadTask = mountainsRef.putBytes(Archivo);
        /*uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {


                listener.OnSetError(exception.getLocalizedMessage());
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getUploadSessionUri();



                listener.OnSetFile(downloadUrl.toString());

            }
        });
        uploadTask = ref.putFile(file);*/
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    listener.OnSetError(task.getException().getLocalizedMessage().toString());
                    throw task.getException();

                }

                listener.OnSetFile(mountainsRef.getDownloadUrl().toString());
                return mountainsRef.getDownloadUrl();

            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();

                    listener.OnSetFile(downloadUri.toString());
                } else {
                    // Handle failures
                    // ...
                    listener.OnSetError("Intentelo mas tarde");
                }
            }
        });
    }
}
