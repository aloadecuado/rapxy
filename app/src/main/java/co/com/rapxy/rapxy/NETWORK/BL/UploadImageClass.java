package co.com.rapxy.rapxy.NETWORK.BL;

import android.graphics.Bitmap;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;

import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.WS.UploadFileClass;


/**
 * Created by pedrodaza on 24/01/18.
 */

public class UploadImageClass {

    public interface OnSetDataImage
    {
        void OnSetImage(String UrlDowload);
    }

    public void UploadImageUser(UserObject userObject, ImageView image, final OnSetDataImage listener)
    {


        String Nombre = userObject.getId() + "Fotos" + userObject.getIdNumber() + "Id" + 0 + ".jpg";
        String Referencia = userObject.getId() + "/" + "Fotos" + "/" + userObject.getIdNumber() +".jpg";



        image.setDrawingCacheEnabled(true);


        Bitmap bmap = image.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        new UploadFileClass().UploadFile(Referencia, Nombre, data, new UploadFileClass.OnSetDataFile() {
            @Override
            public void OnSetFile(String UrlDownload) {
                listener.OnSetImage(UrlDownload);
            }

            @Override
            public void OnSetError(String error) {

            }
        });
    }

    public void  UploadBitmapUser(UserObject userObject, Bitmap bitmap, final OnSetDataImage listener)
    {

        String Nombre = "";
        String Referencia = "";

        if (userObject.getUrlsDocuments() == null)
        {
            Nombre = userObject.getId() + "Documets" + userObject.getIdNumber() + "Id" + 0 + ".jpg";
            Referencia = userObject.getId() + "/" + "Documets" + "/" + userObject.getIdNumber() +".jpg";

        }
        else
        {
            Nombre = userObject.getId() + "Documets" + userObject.getIdNumber() + "Id" + userObject.getUrlsDocuments().size() + ".jpg";
            Referencia = userObject.getId() + "/" + "Documets" + "/" + userObject.getIdNumber() +".jpg";
        }



        Bitmap bmap = bitmap;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        new UploadFileClass().UploadFile(Referencia, Nombre, data, new UploadFileClass.OnSetDataFile() {
            @Override
            public void OnSetFile(String UrlDownload) {
                listener.OnSetImage(UrlDownload);
            }

            @Override
            public void OnSetError(String error) {

            }
        });
    }

    public void  UploadBitmapPhotosUser(UserObject userObject, Bitmap bitmap, final OnSetDataImage listener)
    {

        String Nombre = "";
        String Referencia = "";

        if (userObject.getUrlsPhotos() == null)
        {
            Nombre = userObject.getId() + "Fotos" + userObject.getIdNumber() + "Id" + 0 + ".jpg";
            Referencia = userObject.getId() + "/" + "Fotos" + "/" + userObject.getIdNumber() +".jpg";

        }
        else
        {
            Nombre = userObject.getId() + "Fotos" + userObject.getIdNumber() + "Id" + userObject.getUrlsPhotos().size() + ".jpg";
            Referencia = userObject.getId() + "/" + "Fotos" + "/" + userObject.getIdNumber() +".jpg";
        }



        Bitmap bmap = bitmap;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        new UploadFileClass().UploadFile(Referencia, Nombre, data, new UploadFileClass.OnSetDataFile() {
            @Override
            public void OnSetFile(String UrlDownload) {
                listener.OnSetImage(UrlDownload);
            }

            @Override
            public void OnSetError(String error) {

            }
        });
    }

    public void  UploadBitmapPhotosService(ServicesObject servicesObject, Bitmap bitmap, final OnSetDataImage listener)
    {

        String Nombre = "";
        String Referencia = "";

            Nombre = servicesObject.getId() + "NequiImage" + ".jpg";
            Referencia = servicesObject.getId() + "/" + "NequiImage.jpg";




        Bitmap bmap = bitmap;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        new UploadFileClass().UploadFile(Referencia, Nombre, data, new UploadFileClass.OnSetDataFile() {
            @Override
            public void OnSetFile(String UrlDownload) {
                listener.OnSetImage(UrlDownload);
            }

            @Override
            public void OnSetError(String error) {

            }
        });
    }
}
