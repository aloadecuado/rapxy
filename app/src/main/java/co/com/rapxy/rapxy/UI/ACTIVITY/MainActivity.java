package co.com.rapxy.rapxy.UI.ACTIVITY;

import android.content.Intent;


import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.com.rapxy.rapxy.MODAL.ListObjetc;
import co.com.rapxy.rapxy.MODAL.StateObjetc;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.NETWORK.WS.SetObjectClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.DIALOG.SelectAppDialog;
import co.com.rapxy.rapxy.UTIL.CallStorageResourceClass;
import co.com.rapxy.rapxy.UTIL.WriteReadStorageClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.*;

public class MainActivity extends AppCompatActivity {


    private static final int RC_SIGN_IN = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

          
        /*List<ListObjetc> listObjetcList = new ArrayList<>();

        ListObjetc listObjetc1 = new ListObjetc();
        ListObjetc listObjetc2 = new ListObjetc();

        listObjetc1.setValue("TTR");
        listObjetc1.setValue("Reserva");

        listObjetc2.setValue("TTV");
        listObjetc2.setValue("Viaje inmediato");

        listObjetcList.add(listObjetc1);
        listObjetcList.add(listObjetc2);

        new SetObjectClass().SetObjectKeyForCustomKey(listObjetcList, FIREURL_LIST, FIREURL_LIST_TYPE_TRAVEL, new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {

            }

            @Override
            public void onGetError(String Err) {

            }
        });*/

        /*StateObjetc stateObjetc = new StateObjetc();

        stateObjetc.setDscription("Vehiculo ha llegado");
        stateObjetc.setValue("VLS");

        new SetObjectClass().SetObjectKeyForAutoKey(stateObjetc, FIREURL_STATE, new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {

            }

            @Override
            public void onGetError(String Err) {

            }
        });*/

        final FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {


            floidNavigation(auth);


            /*new CallStorageResourceClass(new CallStorageResourceClass.onGetCallAllStorageRecurse() {
                @Override
                public void onGetStoragesave() {
                    FirebaseUser user = auth.getCurrentUser();
                    UserObject userObject = new UserObject();


                    userObject.setEmail(user.getEmail());
                    userObject.setEmailComprobed(user.getEmail());
                    userObject.setName(user.getDisplayName());
                    userObject.setUid(user.getUid());
                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = tsLong.toString();
                    userObject.setDateRegister(ts);
                    new SetUserClass().createUserKeyForUid(userObject, new SetUserClass.onGetUser() {
                        @Override
                        public void onGetUser(UserObject userObject) {
                            USEROBJECT = userObject;

                            startActivity(new Intent(MainActivity.this, MenuActivity.class));
                            finish();
                        }
                    });
                }
            });*/




            // already signed in
        } else {

            /*startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(
                                    Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                            .setLogo(R.drawable.logo)
                            .setTheme(R.style.LoginTheme)
                            .setIsSmartLockEnabled(false)
                            .build(),
                    RC_SIGN_IN);*/
            // not signed in

            List<AuthUI.IdpConfig> providers = Arrays.asList(
                    new AuthUI.IdpConfig.EmailBuilder().build(),
                    new AuthUI.IdpConfig.GoogleBuilder().build());

// Create and launch sign-in intent
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .setLogo(R.drawable.logo)
                            .setTheme(R.style.LoginTheme)
                            .setIsSmartLockEnabled(false)
                            .build(),
                    RC_SIGN_IN);
        }
    }

    private void floidNavigation(final FirebaseAuth auth)
    {
        FirebaseUser user = auth.getCurrentUser();
        UserObject userObject = new UserObject();


        userObject.setEmail(user.getEmail());
        userObject.setEmailComprobed(user.getEmail());
        userObject.setName(user.getDisplayName());
        userObject.setUid(user.getUid());
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        userObject.setDateRegister(ts);
        new SetUserClass().createUserKeyForUid(MainActivity.this,userObject, new SetUserClass.onGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {
                USEROBJECT = userObject;
                new CallStorageResourceClass(MainActivity.this, new CallStorageResourceClass.onGetCallAllStorageRecurse() {
                    @Override
                    public void onGetStoragesave() {
                        if (new WriteReadStorageClass().getDataString(MainActivity.this, "KeyApp").equals("Sin asignar"))
                        {
                            FragmentManager fm = getSupportFragmentManager();
                            SelectAppDialog editNameDialogFragment = new SelectAppDialog(new SelectAppDialog.onGetSelectAppDialog() {
                                @Override
                                public void onGetUsuarioVehiculo(String tipo) {



                                        startActivity(new Intent(MainActivity.this, MenuActivity.class));
                                        finish();


                                }
                            });
                            editNameDialogFragment.show(fm, "fragment_edit_name");
                        }
                        else if (new WriteReadStorageClass().getDataString(MainActivity.this, "KeyApp").equals("Vehiculo"))
                        {

                            startActivity(new Intent(MainActivity.this, MenuActivity.class));
                            finish();

                        }
                        else if (new WriteReadStorageClass().getDataString(MainActivity.this, "KeyApp").equals("Usuario"))
                        {
                            startActivity(new Intent(MainActivity.this, MenuActivity.class));
                            finish();
                        }
                    }
                });
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {



            // Successfully signed in
            if (resultCode == RESULT_OK) {

                final FirebaseAuth auth = FirebaseAuth.getInstance();
                floidNavigation(auth);



                return;
            } else {


            }


        }

    }
}
