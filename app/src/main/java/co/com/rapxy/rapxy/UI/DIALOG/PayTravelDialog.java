package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.DialogFragment;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.CreditCardObject;
import co.com.rapxy.rapxy.MODAL.PayServiceObject;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetServiceClass;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.NETWORK.BL.UploadImageClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ADAPTER.CreditCardAdapter;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

import static android.app.Activity.RESULT_OK;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;

/**
 * Created by pedroalonsodazab on 25/02/18.
 */
@SuppressLint("ValidFragment")
public class PayTravelDialog extends DialogFragment {

    public interface onGetService
    {
        void onGetService(ServicesObject servicesObject);
    }

    ServicesObject servicesObject;
    onGetService Listener;
    UserObject userObject;
    @SuppressLint("ValidFragment")
    public PayTravelDialog(ServicesObject servicesObject, UserObject userObject, onGetService Listener)
    {
        this.servicesObject = servicesObject;
        this.Listener = Listener;
        this.userObject = userObject;;
    }


    @BindView(R.id.lvCreditCards)
    ListView lvCreditCards;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_pay_travel, null);
        ButterKnife.bind(this, view);

        if (userObject.getCreditCards() != null)
        {
            CreditCardAdapter creditCardAdapter = new CreditCardAdapter(getActivity(), R.layout.adapter_plan, userObject.getCreditCards());
            lvCreditCards.setAdapter(creditCardAdapter);
            lvCreditCards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    final CreditCardObject creditCardObject = userObject.getCreditCards().get(position);

                    PayServiceObject payServiceObject = new PayServiceObject();
                    payServiceObject.setCreditCard(creditCardObject);

                    new SetServiceClass().setServicePayForId(getContext(), servicesObject, payServiceObject, null, new SetServiceClass.onGetService() {
                        @Override
                        public void onGetService(ServicesObject servicesObject) {
                            Listener.onGetService(servicesObject);
                            try
                            {
                                PayTravelDialog.this.getDialog().cancel();
                            }catch (Exception e)
                            {

                            }
                        }
                    });
                    //Listener.onGetService(creditCardObject);

                }
            });
        }
        else
        {
            new AlertsClass().showAlerInfo(getContext(), getContext().getResources().getString(R.string.alert_title_information), getContext().getResources().getString(R.string.alert_message_no_creditcard));
        }
        builder.setView(view);
        return builder.create();
    }

    @OnClick(R.id.btAddPhotoNequi)
    public void addPhotoNequi()
    {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);
        //SelectImageResourceDialog.this.getDialog().cancel();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if(imageReturnedIntent.getData() == null){
            return;
        }
        Uri selectedImage = imageReturnedIntent.getData();
        Bitmap  mBitmap = null;
        try {
            mBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }


        PayServiceObject payServiceObject = new PayServiceObject();
        new SetServiceClass().setServicePayForId(getContext(), servicesObject, payServiceObject, mBitmap, new SetServiceClass.onGetService() {
            @Override
            public void onGetService(ServicesObject servicesObject) {
                Listener.onGetService(servicesObject);

                Listener.onGetService(servicesObject);
                try
                {
                    PayTravelDialog.this.getDialog().cancel();
                }catch (Exception e)
                {

                }
            }
        });

        //listener.onGetImage(mBitmap);
        PayTravelDialog.this.getDialog().cancel();
    }
}
