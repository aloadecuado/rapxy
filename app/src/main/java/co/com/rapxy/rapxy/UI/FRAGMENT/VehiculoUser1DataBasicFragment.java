package co.com.rapxy.rapxy.UI.FRAGMENT;


import android.content.Context;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.LISTIDTYPE;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehiculoUser1DataBasicFragment extends Fragment {

    @BindView(R.id.lyGeneral1)
    LinearLayout lyGeneral1;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etNick)
    EditText etNick;

    @BindView(R.id.etIdNumber)
    EditText etIdNumber;

    @BindView(R.id.etPhoneCell)
    EditText etPhoneCell;

    @BindView(R.id.etObservations)
    EditText etObservations;

    @BindView(R.id.spIdType)
    MaterialSpinner spIdType;

    public VehiculoUser1DataBasicFragment() {


        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vehiculo_user1_data_basic, container, false);

        ButterKnife.bind(this, view);

        String[] asTipoIds = new GlobalConstansAndVarClass().GetArrayString(LISTIDTYPE);

        spIdType.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, asTipoIds));

        SetStringData(USEROBJECT);


        etEmail.setFocusable(false);


        return view;
    }


    private void SetStringData(UserObject userObject)
    {
        etEmail.setText(userObject.getEmailComprobed());
        etEmail.setEnabled(false);
        etName.setText(userObject.getName());
        etNick.setText(userObject.getNick());
        if (userObject.getIdType() != null)
        {
            spIdType.setSelectedIndex(new GlobalConstansAndVarClass().getIndexList(userObject.getIdType().getDescription(), LISTIDTYPE));
        }

        etIdNumber.setText(userObject.getIdNumber());
        etPhoneCell.setText(userObject.getNumberCellphone());
        etObservations.setText(userObject.getDescription());


    }

    private void SetUserObject()
    {
        USEROBJECT.setName(etName.getText().toString());
        USEROBJECT.setNick(etNick.getText().toString());
        USEROBJECT.setIdType(new GlobalConstansAndVarClass().GetListObject(spIdType.getText().toString(), LISTIDTYPE));
        USEROBJECT.setIdNumber(etIdNumber.getText().toString());
        USEROBJECT.setNumberCellphone(etPhoneCell.getText().toString());
        USEROBJECT.setDescription(etObservations.getText().toString());

    }

    @OnClick(R.id.btSave)
    public void Save()
    {
        SetUserObject();

        new SetUserClass().SetUserForid(USEROBJECT, new SetUserClass.onGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {
                USEROBJECT = userObject;
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

}
