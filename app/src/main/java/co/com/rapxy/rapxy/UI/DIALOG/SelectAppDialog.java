package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;

import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.*;

/**
 * Created by pedrodaza on 31/01/18.
 */
@SuppressLint("ValidFragment")
public class SelectAppDialog extends DialogFragment {

    onGetSelectAppDialog listener;

    String Vehiculo = "Vehiculo";
    String Usuario = "Usuario";

    public interface onGetSelectAppDialog
    {
        void onGetUsuarioVehiculo(String tipo);

    }

    @SuppressLint("ValidFragment")
    public SelectAppDialog(onGetSelectAppDialog listener)
    {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_select_app, null);
        ButterKnife.bind(this, view);





        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view);


        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (USEROBJECT != null) {
            if (USEROBJECT.getRol() != null) {

                if (USEROBJECT.getRol().getValue().equals("RU")) {
                    listener.onGetUsuarioVehiculo(Usuario);
                    this.getDialog().cancel();

                } else if (USEROBJECT.getRol().getValue().equals("RV")) {
                    listener.onGetUsuarioVehiculo(Vehiculo);
                    this.getDialog().cancel();

                }
            }
        }
    }

    @OnClick(R.id.btUsuario)
    public void Usuario()
    {
        new SetUserClass().setRolUserForId(getActivity(), USEROBJECT, ROLS.get(0), new SetUserClass.onGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {
                USEROBJECT = userObject;
                listener.onGetUsuarioVehiculo(Vehiculo);
                SelectAppDialog.this.getDialog().cancel();
            }
        });

    }


    @OnClick(R.id.btVehiculo)
    public void Vehiculo()
    {
        new SetUserClass().setRolUserForId(getActivity(), USEROBJECT, ROLS.get(2), new SetUserClass.onGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {
                USEROBJECT = userObject;
                listener.onGetUsuarioVehiculo(Vehiculo);
                SelectAppDialog.this.getDialog().cancel();
            }
        });

    }



}
