package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

/**
 * Created by Pedro on 10/02/2018.
 */
@SuppressLint("ValidFragment")
public class DetailPlanDialog extends DialogFragment {

    Context context; onGetUser listener;UserObject userObject;PlanObject planObject;
    @SuppressLint("ValidFragment")
    public DetailPlanDialog(Context context,UserObject userObject, PlanObject planObject, onGetUser listener)
    {

        this.context = context;
        this.listener = listener;
        this.userObject = userObject;
        this.planObject = planObject;
    }

    public interface onGetUser
    {
        void onGetUser(UserObject userObject);
    }
    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etDescrition)
    EditText etDescrition;

    @BindView(R.id.etNumbersDays)
    EditText etNumbersDays;

    @BindView(R.id.etPrice)
    EditText etPrice;

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_detail_plan, null);
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        etName.setText(planObject.getNombre());
        etDescrition.setText(planObject.getDescription());
        etNumbersDays.setText("" + planObject.getNumberHour());
        etPrice.setText("" + planObject.getPrice());


        builder.setView(view).setPositiveButton(R.string.btn_erase, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                new AlertsClass().showAlerInfoYesOrNo(context, "Información", "¿Desea Eliminar esta imagen?", new AlertsClass.onGetData() {
                    @Override
                    public void onGetDataOk() {
                        new SetUserClass().removePlanUserForId(context, planObject, userObject, new SetUserClass.onGetUser() {
                            @Override
                            public void onGetUser(UserObject userObject) {
                                listener.onGetUser(userObject);
                            }
                        });
                    }

                    @Override
                    public void onGetDataCalcel() {
                        DetailPlanDialog.this.getDialog().cancel();
                    }
                });






            }
        })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        DetailPlanDialog.this.getDialog().cancel();

                    }
                });
        return builder.create();
    }

}
