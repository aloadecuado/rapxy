package co.com.rapxy.rapxy.NETWORK.BL;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.WS.GetObjectClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.*;

/**
 * Created by pedrodaza on 20/02/18.
 */

public class GetServicesClass {

    public interface onGetService
    {
        void onGetService(ServicesObject servicesObject);
        void onNoService();
    }

    public interface onGetServiceUser
    {
        void onGetServiceUser(ServicesObject servicesObject, UserObject userObject);
        void onNoService();
    }

    public interface onGetServicesUser
    {
        void onGetServiceUser(List<ServicesObject> servicesObject);
        void onNoService();
    }
    private static final String TAG = "GetServicesClass";

    public void subscribeAllServiceForId(final Context context, ServicesObject servicesObject, final onGetService listener)
    {



        new GetObjectClass().getListenerObjectForId(FIREURL_SERVICE, servicesObject.getId(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {

                final ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);
                servicesObject1.setId(key);

                if (servicesObject1 != null)
                {
                    listener.onGetService(servicesObject1);

                }
                else
                {
                    //new AlertsClass().hideProggress(progressDialog);
                    listener.onNoService();
                }
            }

            @Override
            public void onGetNullObject() {
                listener.onNoService();
                //new AlertsClass().hideProggress(progressDialog);
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                //new AlertsClass().hideProggress(progressDialog);
            }
        });
    }

    public void getAllSercicesFull(final Context context, UserObject userObject, final onGetServicesUser listener){

        String idAtrr = "";

        if (userObject.getRol() == null)
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_rol));
            listener.onNoService();
            return;
        }
        else if (userObject.getRol().getValue().equals("RU"))
        {
            idAtrr = "idUser";

        }else if (userObject.getRol().getValue().equals("RV"))
        {
            idAtrr = "idDriver";
        }

        new GetObjectClass().getListenerListObjectEqualsString(FIREURL_SERVICE, idAtrr, userObject.getId(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                ArrayList<ServicesObject> servicesObjects =  new ArrayList<ServicesObject>();
                for (DataSnapshot snapshot1: snapshot.getChildren())
                {
                    ServicesObject servicesObject1 = snapshot1.getValue(ServicesObject.class);
                    servicesObject1.setId(snapshot1.getKey());

                    servicesObjects.add(servicesObject1);

                }

                if(servicesObjects.size() >= 1){
                    listener.onGetServiceUser(servicesObjects);
                }else{
                    listener.onNoService();
                }
            }

            @Override
            public void onGetNullObject() {
                listener.onNoService();
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
            }
        });
    }


    public void getAllSercices(final Context context, UserObject userObject, final onGetServicesUser listener){

        String idAtrr = "";

        if (userObject.getRol() == null)
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_rol));
            listener.onNoService();
            return;
        }
        else if (userObject.getRol().getValue().equals("RU"))
        {
            idAtrr = "idUser";

        }else if (userObject.getRol().getValue().equals("RV"))
        {
            idAtrr = "idDriver";
        }

        new GetObjectClass().getListenerListObjectEqualsString(FIREURL_SERVICE, idAtrr, userObject.getId(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                ArrayList<ServicesObject> servicesObjects =  new ArrayList<ServicesObject>();
                for (DataSnapshot snapshot1: snapshot.getChildren())
                {
                    ServicesObject servicesObject1 = snapshot1.getValue(ServicesObject.class);
                    servicesObject1.setId(snapshot1.getKey());

                    if (servicesObject1.getState() != null){
                        if (!servicesObject1.getState().getValue().equals("NS") && !servicesObject1.getState().getValue().equals("LS")){

                            servicesObjects.add(servicesObject1);
                        }
                    }
                }

                if(servicesObjects.size() >= 1){
                    listener.onGetServiceUser(servicesObjects);
                }else{
                    listener.onNoService();
                }
            }

            @Override
            public void onGetNullObject() {
                listener.onNoService();
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
            }
        });
    }
    public void getLastServiceVigenteVehiculo(final Context context, final UserObject userObject, final onGetServiceUser listener)
    {
        String idAtrr = "";

        if (userObject.getRol() == null)
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_rol));
            listener.onNoService();
            return;
        }
        else if (userObject.getRol().getValue().equals("RU"))
        {
            idAtrr = "idUser";

        }else if (userObject.getRol().getValue().equals("RV"))
        {
            idAtrr = "idDriver";
        }
        else if (userObject.getRol().getValue().equals("RH"))
        {
            idAtrr = "idDriver";
        }
        //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(FIREURL_SERVICE, idAtrr, userObject.getId(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {


                ServicesObject servicesObject = null;
                for (DataSnapshot snapshot1: snapshot.getChildren())
                {
                    ServicesObject servicesObject1 = snapshot1.getValue(ServicesObject.class);
                    servicesObject1.setId(snapshot1.getKey());
                    if (servicesObject1.getType() != null)
                    {
                        if (servicesObject1.getType().getValue().toString().equals("LTTV"))
                        {
                            if (servicesObject1.getState() != null)
                            {
                                if (servicesObject1.getState().getValue().toString().equals("FS") || servicesObject1.getState().getValue().toString().equals("NS"))
                                {

                                }
                                else
                                {
                                    servicesObject = snapshot1.getValue(ServicesObject.class);
                                    servicesObject.setId(snapshot1.getKey());

                                    break;
                                }
                            }
                        }
                    }

                }
                Log.v(TAG, "Service =" + servicesObject);
                if (servicesObject == null)
                {
                    //new AlertsClass().hideProggress(progressDialog);
                    listener.onNoService();
                }
                else
                {
                    //new AlertsClass().hideProggress(progressDialog);
                    final ServicesObject finalServicesObject = servicesObject;
                    new GetObjectClass().getSimpleObjectsForKey(FIREURL_USERS, finalServicesObject.getIdUser(), new GetObjectClass.onGetObject() {
                        @Override
                        public void onGetObject(DataSnapshot snapshot, String key) {
                            Log.v(TAG, "DataSnapshot user service =" + snapshot);
                            UserObject userObject1 = snapshot.getValue(UserObject.class);
                            userObject1.setId(key);
                            //new AlertsClass().hideProggress(progressDialog);
                            listener.onGetServiceUser(finalServicesObject, userObject1);
                        }

                        @Override
                        public void onGetNullObject() {
                            //new AlertsClass().hideProggress(progressDialog);
                        }

                        @Override
                        public void OnGetError(String Err) {
                            //new AlertsClass().hideProggress(progressDialog);
                        }
                    });
                    // se llama user


                }

            }

            @Override
            public void onGetNullObject() {
                listener.onNoService();
                //new AlertsClass().hideProggress(progressDialog);
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                //new AlertsClass().hideProggress(progressDialog);
            }
        });
    }


    public void getLastServiceVigenteUser(final Context context, UserObject userObject, final onGetServiceUser listener)
    {
        String puta = "";
        //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(FIREURL_SERVICE, "idUser", userObject.getId(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {


                ServicesObject servicesObject = null;
                for (DataSnapshot snapshot1: snapshot.getChildren())
                {
                    ServicesObject servicesObject1 = snapshot1.getValue(ServicesObject.class);
                    servicesObject1.setId(snapshot1.getKey());
                    if (servicesObject1.getType() != null)
                    {
                        if (servicesObject1.getType().getValue().equals("LTTV"))
                        {
                            if (servicesObject1.getState() != null)
                            {
                                if (servicesObject1.getState().getValue().equals("FS") || servicesObject1.getState().getValue().equals("NS"))
                                {

                                }
                                else
                                {
                                    servicesObject = snapshot1.getValue(ServicesObject.class);
                                    servicesObject.setId(snapshot1.getKey());

                                    break;
                                }
                            }
                        }
                    }

                }
                Log.v(TAG, "Service =" + servicesObject);
                if (servicesObject == null)
                {
                    //new AlertsClass().hideProggress(progressDialog);
                    listener.onNoService();
                }
                else
                {
                    //new AlertsClass().hideProggress(progressDialog);
                    final ServicesObject finalServicesObject = servicesObject;
                    new GetObjectClass().getSimpleObjectsForKey(FIREURL_USERS, finalServicesObject.getIdUser(), new GetObjectClass.onGetObject() {
                        @Override
                        public void onGetObject(DataSnapshot snapshot, String key) {
                            Log.v(TAG, "DataSnapshot user service =" + snapshot);
                            UserObject userObject1 = snapshot.getValue(UserObject.class);
                            userObject1.setId(key);
                            listener.onGetServiceUser(finalServicesObject, userObject1);
                        }

                        @Override
                        public void onGetNullObject() {
                            //new AlertsClass().hideProggress(progressDialog);
                        }

                        @Override
                        public void OnGetError(String Err) {
                            //new AlertsClass().hideProggress(progressDialog);
                        }
                    });
                    // se llama user


                }

            }

            @Override
            public void onGetNullObject() {
                listener.onNoService();
                //new AlertsClass().hideProggress(progressDialog);
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                //new AlertsClass().hideProggress(progressDialog);
            }
        });
    }

    public void subscribeServiceForId(final Context context, ServicesObject servicesObject, UserObject userObject, final onGetServiceUser listener)
    {
        String idUser = "";

        if (userObject.getRol() == null)
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_rol));
            listener.onNoService();
            return;
        }
        else if (userObject.getRol().getValue().equals("RU"))
        {
            idUser = servicesObject.getIdDriver();

        }else if (userObject.getRol().getValue().equals("RV"))
        {
            idUser = servicesObject.getIdUser();
        }
        else if (userObject.getRol().getValue().equals("RH")) {
            idUser = servicesObject.getIdUser();
        }

            //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        final String finalIdUser = idUser;
        new GetObjectClass().getListenerObjectForId(FIREURL_SERVICE, servicesObject.getId(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {

                final ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);
                servicesObject1.setId(key);

                if (servicesObject1 != null)
                {
                    new GetUserClass().getUserForId(context, finalIdUser, new GetUserClass.onGetUser() {
                        @Override
                        public void onGetUser(UserObject userObjects) {
                            //new AlertsClass().hideProggress(progressDialog);
                            listener.onGetServiceUser(servicesObject1, userObjects);
                        }
                    });

                }
                else
                {
                    //new AlertsClass().hideProggress(progressDialog);
                    listener.onNoService();
                }
            }

            @Override
            public void onGetNullObject() {
                listener.onNoService();
                //new AlertsClass().hideProggress(progressDialog);
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                //new AlertsClass().hideProggress(progressDialog);
            }
        });
    }



}
