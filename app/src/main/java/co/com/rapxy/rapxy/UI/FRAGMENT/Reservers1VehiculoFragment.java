package co.com.rapxy.rapxy.UI.FRAGMENT;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.GetUserClass;
import co.com.rapxy.rapxy.NETWORK.BL.SetServiceClass;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ADAPTER.VehiculoAdapter;
import co.com.rapxy.rapxy.UI.DIALOG.AddReserverVehiculoDialog;
import co.com.rapxy.rapxy.UI.DIALOG.DetailTravelDialog;
import co.com.rapxy.rapxy.UI.DIALOG.ListImageGalleryDialog;
import co.com.rapxy.rapxy.UI.DIALOG.PayTravelDialog;
import co.com.rapxy.rapxy.UI.DIALOG.TravelVehiculoDialog;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.HOTELS;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.SERVICEOBJECT;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECTSERVICE;


public class Reservers1VehiculoFragment extends Fragment {


    public Reservers1VehiculoFragment() {
        // Required empty public constructor
    }


    private RecyclerView rvListVehiculos;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reservers1_vehiculo, container, false);
        rvListVehiculos = (RecyclerView) view.findViewById(R.id.rvListVehiculos);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rvListVehiculos.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        rvListVehiculos.setLayoutManager(mLayoutManager);


        // specify an adapter (see also next example)
        setAdapter();
        // Inflate the layout for this fragment
        return view;
    }

    private void setAdapter()
    {
        if (USEROBJECT.getUrlsDocuments() != null)
        {
            String[] Documents = new String[USEROBJECT.getUrlsDocuments().size()];
            int i = 0;
            for (String Description : USEROBJECT.getUrlsDocuments())
            {
                Documents[i] = Description;

                i++;
            }

            new GetUserClass().getListListenerUsersForStateActivo(getActivity(), USEROBJECT, new GetUserClass.onGetUsers() {
                @Override
                public void onGetListUsers(List<UserObject> userObjects) {
                    mAdapter = new VehiculoAdapter(USEROBJECT, userObjects, getActivity(), new VehiculoAdapter.onGetUser() {
                        @Override
                        public void onGetUser(final UserObject userObject) {
                            USEROBJECTSERVICE = userObject;
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            AddReserverVehiculoDialog addReserverVehiculoDialog = new AddReserverVehiculoDialog(HOTELS, USEROBJECT, userObject, new AddReserverVehiculoDialog.onGetUserAddresVehiculo() {
                                @Override
                                public void onGetUser(AddresObject addresObject, UserObject userObject, UserObject userObjectVehiculo, PlanObject planObject) {


                                }
                            });
                            addReserverVehiculoDialog.show(fragmentManager, "AddReserverVehiculoDialog");
                            /*DetailTravelDialog editNameDialogFragment = new DetailTravelDialog(HOTELS, USEROBJECT, userObject, new DetailTravelDialog.onGetUserAddresVehiculo() {
                                @Override
                                public void onGetUser(AddresObject addresObject, UserObject userObject, UserObject userObjectVehiculo, PlanObject planObject) {
                                    final ServicesObject servicesObject = new ServicesObject();
                                    servicesObject.setAddress(addresObject);
                                    servicesObject.setPlan(planObject);
                                    servicesObject.setIdDriver(userObjectVehiculo.getId());
                                    servicesObject.setIdUser(userObject.getId());
                                    servicesObject.setLat(addresObject.getLat());
                                    servicesObject.setLot(addresObject.getLog());
                                    new AlertsClass().showAlerInfoYesOrNo(getContext(), getResources().getString(R.string.alert_title_information), getResources().getString(R.string.alert_message_send_prospa), new AlertsClass.onGetData() {
                                        @Override
                                        public void onGetDataOk() {
                                            new SetServiceClass().addServiceImnediateForUserAndVehiculo(getContext(), servicesObject, new SetServiceClass.onGetService() {
                                                @Override
                                                public void onGetService(ServicesObject servicesObject) {

                                                    SERVICEOBJECT = servicesObject;

                                                    FragmentManager fragmentManager = getFragmentManager();
                                                    TravelVehiculoDialog travelUserDialog = new TravelVehiculoDialog(SERVICEOBJECT,USEROBJECT, USEROBJECTSERVICE, new TravelVehiculoDialog.onGetUserAddresVehiculo() {
                                                        @Override
                                                        public void onGetUser(ServicesObject servicesObject) {
                                                            SERVICEOBJECT = servicesObject;
                                                        }

                                                        @Override
                                                        public void onGetPayService(ServicesObject servicesObject) {

                                                            SERVICEOBJECT = servicesObject;
                                                            FragmentManager fragmentManager1 = getFragmentManager();
                                                            PayTravelDialog payTravelDialog = new PayTravelDialog(SERVICEOBJECT, USEROBJECT, new PayTravelDialog.onGetService() {
                                                                @Override
                                                                public void onGetService(ServicesObject servicesObject) {
                                                                    SERVICEOBJECT = servicesObject;
                                                                }
                                                            });

                                                            payTravelDialog.show(fragmentManager1, "PayTravelDialog");

                                                        }
                                                    });
                                                    travelUserDialog.show(fragmentManager, "TravelUserDialog");
                                                }
                                            });

                                        }

                                        @Override
                                        public void onGetDataCalcel() {

                                        }
                                    });
                                }
                            });
                            editNameDialogFragment.show(fm, "fragment_edit_name");*/

                        }

                        @Override
                        public void onGetListImage(UserObject userObject) {

                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            ListImageGalleryDialog editNameDialogFragment = new ListImageGalleryDialog(userObject.getUrlsPhotos(), getContext());
                            editNameDialogFragment.show(fm, "fragment_edit_name");

                        }

                        @Override
                        public void onGetUserFavorite(UserObject userObject, boolean favorite) {

                            new SetUserClass().addAndRemoveFavoriteUserForId(getActivity(), USEROBJECT, userObject, new SetUserClass.onGetUser() {
                                @Override
                                public void onGetUser(UserObject userObject) {
                                    USEROBJECT = userObject;

                                    setAdapter();
                                }
                            });

                        }
                    });

                    rvListVehiculos.setAdapter(mAdapter);
                }
            });
            /*mAdapter = new VehiculoAdapter()

            mRecyclerView.setAdapter(mAdapter);*/
        }
    }


}
