package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

/**
 * Created by Pedro on 05/02/2018.
 */
@SuppressLint("ValidFragment")
public class DetailImageDialog extends DialogFragment {

    public interface onGetUser
    {
        void onGetUser(UserObject userObject);
    }


    @BindView(R.id.imData)
    ImageView imData;

    UserObject USEROBJECT;

    onGetUser listener;
    String URLIMAGE = "";
    boolean bDocument = false;
    Context context;
    @SuppressLint("ValidFragment")
    public DetailImageDialog(Context context, String urlImage, UserObject userObject, boolean bDocument, onGetUser onGetUser)
    {

        URLIMAGE = urlImage;
        this.USEROBJECT = userObject;
        listener = onGetUser;
        this.bDocument = bDocument;
        this.context = context;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_detail_image, null);
        ButterKnife.bind(this, view);

        Glide.with(context).load(URLIMAGE).into(imData);


        builder.setView(view)


                // Add action buttons
                .setPositiveButton(R.string.btn_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {


                        DetailImageDialog.this.getDialog().cancel();


                    }
                })
                .setNegativeButton(R.string.btn_erase, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new AlertsClass().showAlerInfoYesOrNo(getContext(), "Información", "¿Desea Eliminar esta imagen?", new AlertsClass.onGetData() {
                            @Override
                            public void onGetDataOk() {

                                new SetUserClass().removeUrlImageForId(getContext(), bDocument, USEROBJECT, URLIMAGE, new SetUserClass.onGetUser() {
                                    @Override
                                    public void onGetUser(UserObject userObject) {
                                        listener.onGetUser(userObject);
                                    }
                                });

                            }

                            @Override
                            public void onGetDataCalcel() {

                            }
                        });
                    }
                });
        return builder.create();
    }
}
