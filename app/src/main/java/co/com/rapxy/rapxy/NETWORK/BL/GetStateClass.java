package co.com.rapxy.rapxy.NETWORK.BL;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import co.com.rapxy.rapxy.MODAL.StateObjetc;
import co.com.rapxy.rapxy.NETWORK.WS.GetObjectClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.FIREURL_STATE;


/**
 * Created by pedrodaza on 22/01/18.
 */

public class GetStateClass {

    public interface onGetState
    {
        void onGetState(StateObjetc stateObjetc);

    }

    public interface onGetListState
    {
        void onGetState(List<StateObjetc> stateObjetcs);

    }
    public void GetStateForValue(String Value, final onGetState listener)
    {
        new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(FIREURL_STATE, "value", Value, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {

                StateObjetc stateObjetc = null;
                String Key = "";
                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    stateObjetc = snapshot1.getValue(StateObjetc.class);
                    Key =  snapshot1.getKey();
                }

                if (stateObjetc != null)
                {
                    stateObjetc.setId(Key);
                    listener.onGetState(stateObjetc);
                }
            }

            @Override
            public void onGetNullObject() {

            }

            @Override
            public void OnGetError(String Err) {

            }
        });

    }

    public void GetAllStates(final onGetListState listener)
    {
        new GetObjectClass().getSimpleObjectsforAllChield(FIREURL_STATE, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                ArrayList<StateObjetc> stateObjetcs = new ArrayList<>();

                for(DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    StateObjetc stateObjetc = snapshot1.getValue(StateObjetc.class);
                    stateObjetc.setId(snapshot1.getKey());
                    stateObjetcs.add(stateObjetc);
                }

                if (stateObjetcs.size() >= 1)
                {
                    listener.onGetState(stateObjetcs);
                }
                else
                {

                }
            }

            @Override
            public void onGetNullObject() {

            }

            @Override
            public void OnGetError(String Err) {

            }
        });
    }
}
