package co.com.rapxy.rapxy.NETWORK.BL;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;


import co.com.rapxy.rapxy.MODAL.ListObjetc;
import co.com.rapxy.rapxy.NETWORK.WS.GetObjectClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.FIREURL_LIST;


/**
 * Created by pedrodaza on 22/01/18.
 */

public class GetListClass {

    public interface onGetList
    {
        void onGetList(List<ListObjetc> listObjetca);
    }

    public void GetListForKey(String Key, final onGetList listener)
    {

        new GetObjectClass().getSimpleObjectsForKey(FIREURL_LIST, Key, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                ArrayList<ListObjetc> listObjetcs = new ArrayList<>();

                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    ListObjetc listObjetc = snapshot1.getValue(ListObjetc.class);
                    listObjetc.setId(snapshot1.getKey());
                    listObjetcs.add(listObjetc);


                }

                if (listObjetcs.size() >= 1)
                {
                    listener.onGetList(listObjetcs);
                }
                else
                {

                }
            }

            @Override
            public void onGetNullObject() {

            }

            @Override
            public void OnGetError(String Err) {

            }
        });

    }
}
