package co.com.rapxy.rapxy.MODAL;

/**
 * Created by pedroalonsodazab on 25/02/18.
 */

public class PayServiceObject {

    public CreditCardObject getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCardObject creditCard) {
        this.creditCard = creditCard;
    }

    public String getUrlNequiImage() {
        return urlNequiImage;
    }

    public void setUrlNequiImage(String urlNequiImage) {
        this.urlNequiImage = urlNequiImage;
    }

    public StateObjetc getState() {
        return state;
    }

    public void setState(StateObjetc state) {
        this.state = state;
    }

    CreditCardObject creditCard = null;
    String urlNequiImage = "";
    StateObjetc state = null;
}
