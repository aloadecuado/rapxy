package co.com.rapxy.rapxy.NETWORK.BL;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;

import com.google.firebase.database.DataSnapshot;

import co.com.rapxy.rapxy.MODAL.PayServiceObject;
import co.com.rapxy.rapxy.MODAL.QualifyObject;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.WS.GetObjectClass;
import co.com.rapxy.rapxy.NETWORK.WS.SetObjectClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.FIREURL_SERVICE;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.LISTTYPESTRAVEL;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.STATES;

/**
 * Created by pedrodaza on 20/02/18.
 */

public class SetServiceClass {
    public interface onGetService
    {
        void onGetService(ServicesObject servicesObject);
    }

    public void setServiceQualify(final Context context, UserObject userObject, final ServicesObject servicesObject, final QualifyObject qualifyObject, final onGetService listener)
    {
        if (servicesObject.getState().getValue().equals("FS"))
        {
            if(userObject.getRol().getValue().equals("RU")){

                servicesObject.setQualifyObjectVehiculo(qualifyObject);
            }else if(userObject.getRol().getValue().equals("RU")){
                servicesObject.setQualifyObjectClient(qualifyObject);
            }

            servicesObject.setState(STATES.get(9));
            //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
            new SetObjectClass().SetObjectKeyForCustomKey(servicesObject, FIREURL_SERVICE, servicesObject.getId(), new SetObjectClass.onGetObject() {
                @Override
                public void onGetObject(DataSnapshot snapshot) {

                    ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);

                    if (servicesObject1 != null)
                    {
                        //new AlertsClass().hideProggress(progressDialog);
                        listener.onGetService(servicesObject1);
                    }
                    else
                    {
                        new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_peding));
                        //new AlertsClass().hideProggress(progressDialog);
                    }

                }

                @Override
                public void onGetError(String Err) {
                    new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                    //new AlertsClass().hideProggress(progressDialog);
                }
            });
        }
        else
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_travel_no_pedding));
        }
    }

    public void addServiceImnediateForUserAndVehiculo(final Context context, final ServicesObject servicesObject, final onGetService listener)
    {
        servicesObject.setType(LISTTYPESTRAVEL.get(1));
        servicesObject.setState(STATES.get(4));
        //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(FIREURL_SERVICE, "idUser", servicesObject.getIdUser(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                Boolean bTravelPeding = false;
                for (DataSnapshot snapshot1: snapshot.getChildren())
                {
                    ServicesObject servicesObject1 = snapshot1.getValue(ServicesObject.class);
                    if (servicesObject1.getType() != null)
                    {
                        if (servicesObject1.getType().getValue().toString().equals("LTTV"))
                        {
                            if (servicesObject1.getState() != null)
                            {
                                if (servicesObject1.getState().getValue().toString().equals("FS") || servicesObject1.getState().getValue().toString().equals("NS"))
                                {

                                }
                                else
                                {
                                    bTravelPeding = true;
                                    break;
                                }
                            }
                        }
                    }

                }

                if(bTravelPeding)
                {
                    new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_peding));
                    //new AlertsClass().hideProggress(progressDialog);
                }
                else
                {
                    new SetObjectClass().SetObjectKeyForAutoKey(servicesObject, FIREURL_SERVICE, new SetObjectClass.onGetObject() {
                        @Override
                        public void onGetObject(DataSnapshot snapshot) {
                            //new AlertsClass().hideProggress(progressDialog);
                            ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);
                            servicesObject1.setId(snapshot.getKey());
                            listener.onGetService(servicesObject1);
                        }

                        @Override
                        public void onGetError(String Err) {
                            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                            //new AlertsClass().hideProggress(progressDialog);

                        }
                    });
                }
            }

            @Override
            public void onGetNullObject() {

                new SetObjectClass().SetObjectKeyForAutoKey(servicesObject, FIREURL_SERVICE, new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {
                        //new AlertsClass().hideProggress(progressDialog);
                        ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);
                        servicesObject1.setId(snapshot.getKey());
                        listener.onGetService(servicesObject1);
                    }

                    @Override
                    public void onGetError(String Err) {
                        new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                        //new AlertsClass().hideProggress(progressDialog);

                    }
                });


            }

            @Override
            public void OnGetError(String Err) {

            }
        });


    }

    public void setServiceAceptForId(final Context context, final ServicesObject servicesObject, final onGetService listener)
    {
        if (servicesObject.getState().getValue().equals("ES") || servicesObject.getState().getValue().equals("CS"))
        {
            servicesObject.setState(STATES.get(6));
            //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
            new SetObjectClass().SetObjectKeyForCustomKey(servicesObject, FIREURL_SERVICE, servicesObject.getId(), new SetObjectClass.onGetObject() {
                @Override
                public void onGetObject(DataSnapshot snapshot) {

                    ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);

                    if (servicesObject1 != null)
                    {
                        //new AlertsClass().hideProggress(progressDialog);
                        listener.onGetService(servicesObject1);
                    }
                    else
                    {
                        new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_peding));
                        //new AlertsClass().hideProggress(progressDialog);
                    }

                }

                @Override
                public void onGetError(String Err) {
                    new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                    //new AlertsClass().hideProggress(progressDialog);
                }
            });
        }
        else
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_travel_no_pedding));
        }
    }

    public void setServicePayForId(final Context context, final ServicesObject servicesObject, final PayServiceObject payServiceObject, Bitmap bitmap, final onGetService listener)
    {
        if (servicesObject.getState().getValue().equals("TS"))
        {
            servicesObject.setState(STATES.get(10));

            if (payServiceObject.getCreditCard() != null && bitmap == null) {
                servicesObject.setPayService(payServiceObject);
                //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
                new SetObjectClass().SetObjectKeyForCustomKey(servicesObject, FIREURL_SERVICE, servicesObject.getId(), new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {

                        ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);

                        if (servicesObject1 != null) {
                            //new AlertsClass().hideProggress(progressDialog);
                            listener.onGetService(servicesObject1);
                        } else {
                            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_peding));
                            //new AlertsClass().hideProggress(progressDialog);
                        }

                    }

                    @Override
                    public void onGetError(String Err) {
                        new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                        //new AlertsClass().hideProggress(progressDialog);
                    }
                });
            }
            else if (payServiceObject.getCreditCard() == null && bitmap != null)
            {

                servicesObject.setState(STATES.get(10));
                //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
                new UploadImageClass().UploadBitmapPhotosService(servicesObject, bitmap, new UploadImageClass.OnSetDataImage() {
                    @Override
                    public void OnSetImage(String UrlDowload) {
                        payServiceObject.setUrlNequiImage(UrlDowload);
                        servicesObject.setPayService(payServiceObject);
                        new SetObjectClass().SetObjectKeyForCustomKey(servicesObject, FIREURL_SERVICE, servicesObject.getId(), new SetObjectClass.onGetObject() {
                            @Override
                            public void onGetObject(DataSnapshot snapshot) {

                                ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);

                                if (servicesObject1 != null) {
                                    //new AlertsClass().hideProggress(progressDialog);
                                    listener.onGetService(servicesObject1);
                                } else {
                                    new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_peding));
                                    //new AlertsClass().hideProggress(progressDialog);
                                }

                            }

                            @Override
                            public void onGetError(String Err) {
                                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                                //new AlertsClass().hideProggress(progressDialog);
                            }
                        });
                    }
                });


            }
        }
        else
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_travel_no_aceept));
        }
    }

    public void setServiceIArrivedForId(final Context context, final ServicesObject servicesObject, final onGetService listener)
    {
        if (servicesObject.getState().getValue().equals("PPS") || servicesObject.getState().getValue().equals("PS"))
        {
            servicesObject.setState(STATES.get(11));

                //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
                new SetObjectClass().SetObjectKeyForCustomKey(servicesObject, FIREURL_SERVICE, servicesObject.getId(), new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {

                        ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);

                        if (servicesObject1 != null) {
                            listener.onGetService(servicesObject1);
                            //new AlertsClass().hideProggress(progressDialog);

                        } else {
                            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_peding));
                            //new AlertsClass().hideProggress(progressDialog);
                        }

                    }

                    @Override
                    public void onGetError(String Err) {
                        new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                        //new AlertsClass().hideProggress(progressDialog);
                    }
                });

        }
        else
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_travel_no_aceept));
        }
    }

    public void setServiceForId(final Context context, final ServicesObject servicesObject, final onGetService listener)
    {
        if (servicesObject.getState().getValue().equals("VLS"))
        {
            servicesObject.setState(STATES.get(7));

            //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
            new SetObjectClass().SetObjectKeyForCustomKey(servicesObject, FIREURL_SERVICE, servicesObject.getId(), new SetObjectClass.onGetObject() {
                @Override
                public void onGetObject(DataSnapshot snapshot) {

                    ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);

                    if (servicesObject1 != null) {
                        //new AlertsClass().hideProggress(progressDialog);
                        listener.onGetService(servicesObject1);
                    } else {
                        new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_peding));
                        //new AlertsClass().hideProggress(progressDialog);
                    }

                }

                @Override
                public void onGetError(String Err) {
                    new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                    //new AlertsClass().hideProggress(progressDialog);
                }
            });

        }
        else
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_travel_no_aceept));
        }
    }

    public void setServiceCancelForId(final Context context, final ServicesObject servicesObject, final onGetService listener)
    {
        if (servicesObject.getState().getValue().equals("ES") || servicesObject.getState().getValue().equals("CS") || servicesObject.getState().getValue().equals("TS")  || servicesObject.getState().getValue().equals("PPS"))
        {
            servicesObject.setState(STATES.get(8));

            //final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
            new SetObjectClass().SetObjectKeyForCustomKey(servicesObject, FIREURL_SERVICE, servicesObject.getId(), new SetObjectClass.onGetObject() {
                @Override
                public void onGetObject(DataSnapshot snapshot) {

                    ServicesObject servicesObject1 = snapshot.getValue(ServicesObject.class);

                    if (servicesObject1 != null) {
                        listener.onGetService(servicesObject1);
                        //new AlertsClass().hideProggress(progressDialog);

                    } else {
                        new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_no_peding));
                        //new AlertsClass().hideProggress(progressDialog);
                    }

                }

                @Override
                public void onGetError(String Err) {
                    new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                    //new AlertsClass().hideProggress(progressDialog);
                }
            });

        }
        else
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_travel_no_cancel));
        }
    }


}
