package co.com.rapxy.rapxy.MODAL;

/**
 * Created by Pedro on 11/02/2018.
 */

public class RolObject {
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String descripcion = "";
    private String id = "";
    private String value = "";
}
