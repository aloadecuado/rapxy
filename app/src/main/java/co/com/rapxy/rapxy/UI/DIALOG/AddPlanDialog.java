package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import androidx.fragment.app.DialogFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.rapxy.rapxy.NETWORK.BL.GetPlanClass;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ADAPTER.PlanAdapter;
import co.com.rapxy.rapxy.UTIL.AlertsClass;


/**
 * Created by pedrodaza on 8/02/18.
 */
@SuppressLint("ValidFragment")
public class AddPlanDialog extends DialogFragment {

    public interface onGetUser
    {
        void onGetUser(UserObject userObject);
    }

    Context context; onGetUser listener;UserObject userObject;
    @SuppressLint("ValidFragment")
    public AddPlanDialog(Context context,UserObject userObject, onGetUser listener)
    {

        this.context = context;
        this.listener = listener;
        this.userObject = userObject;
    }

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etDescrition)
    EditText etDescrition;

    @BindView(R.id.etNumbersDays)
    EditText etNumbersDays;

    @BindView(R.id.etPrice)
    EditText etPrice;

    @BindView(R.id.lvPlans)
    ListView lvPlans;

    private PlanObject planObject;
    public AddPlanDialog co = null;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_add_plan, null);
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        new GetPlanClass().getPlanForUserId(context, userObject, new GetPlanClass.onGetPlans() {
            @Override
            public void onGetPlans(final List<PlanObject> planObjects) {


                PlanAdapter planAdapter = new PlanAdapter(getActivity(), R.layout.adapter_plan, planObjects);
                lvPlans.setAdapter(planAdapter);
                lvPlans.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        PlanObject planObject = planObjects.get(position);
                        etName.setText(planObject.getNombre());
                        etDescrition.setText(planObject.getDescription());
                        etNumbersDays.setText("" + planObject.getNumberHour());
                        AddPlanDialog.this.planObject = planObject;
                    }


                });
                if(planObjects.size() >= 1)
                {
                    etName.setText(planObjects.get(0).getNombre());
                    etDescrition.setText(planObjects.get(0).getDescription());
                    etNumbersDays.setText("" + planObjects.get(0).getNumberHour());
                    //co =
                    AddPlanDialog.this.planObject = planObjects.get(0);
                }


            }
        });



        co = this;

        builder.setView(view).setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        if(etPrice.getText().toString().equals(""))
                        {
                            new AlertsClass().showAlerInfo(getContext(),"Información", "Por favor indique el precio.");
                            return;
                        }
                        try
                        {
                         int price = Integer.parseInt(etPrice.getText().toString());

                            AddPlanDialog.this.planObject.setPrice(price);


                        }
                        catch (Exception e)
                        {

                        }

                        new SetUserClass().addPlanUser(getContext(), AddPlanDialog.this.planObject, userObject, new SetUserClass.onGetUser() {
                            @Override
                            public void onGetUser(UserObject userObject) {
                                listener.onGetUser(userObject);
                                //co.getDialog().cancel();
                            }
                        });





                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        AddPlanDialog.this.getDialog().cancel();

                    }
                });
        return builder.create();
    }
}
