package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;

import androidx.fragment.app.DialogFragment;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.CreditCardObject;
import co.com.rapxy.rapxy.MODAL.PayServiceObject;
import co.com.rapxy.rapxy.MODAL.QualifyObject;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetServiceClass;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ADAPTER.CreditCardAdapter;
import co.com.rapxy.rapxy.UTIL.AlertsClass;
import co.com.rapxy.rapxy.UTIL.ValidationClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;

/**
 * Created by pedroalonsodazab on 25/02/18.
 */
@SuppressLint("ValidFragment")
public class QualifyDialog extends DialogFragment {

    public interface onGetService
    {
        void onGetService(ServicesObject servicesObject);
    }

    ServicesObject servicesObject;
    onGetService Listener;
    UserObject userObject;
    Context context;
    @SuppressLint("ValidFragment")
    public QualifyDialog(Context context, ServicesObject servicesObject, UserObject userObject, onGetService Listener)
    {
        this.servicesObject = servicesObject;
        this.Listener = Listener;
        this.userObject = userObject;
        this.context = context;
    }


    @BindView(R.id.tbQualify)
    RatingBar tbQualify;

    @BindView(R.id.etObservations)
    EditText etObservations;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_qualify, null);
        ButterKnife.bind(this, view);


        tbQualify.setNumStars(2);
        builder.setView(view)
                .setPositiveButton(R.string.btn_qualify, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        QualifyObject qualifyObject = new QualifyObject();
                        qualifyObject.setQualify((int) tbQualify.getRating());
                        qualifyObject.setObservation(etObservations.getText().toString());
                        new SetServiceClass().setServiceQualify(context, userObject, servicesObject, qualifyObject, new SetServiceClass.onGetService() {
                            @Override
                            public void onGetService(ServicesObject servicesObject) {

                                Listener.onGetService(servicesObject);
                                dialog.cancel();

                            }
                        });

                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }
                });

        return builder.create();
    }


}
