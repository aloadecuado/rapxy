package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.DialogFragment;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass;

import static android.app.Activity.RESULT_OK;

/**
 * Created by pedrodaza on 31/01/18.
 */

@SuppressLint("ValidFragment")
public class SelectImageResourceDialog  extends DialogFragment {

    onGetImage listener;

    public interface onGetImage
    {
        void onGetImage(Bitmap bitmap);

    }

    @SuppressLint("ValidFragment")
    public SelectImageResourceDialog(onGetImage listener)
    {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_select_image_storage, null);
        ButterKnife.bind(this, view);





        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view);
        return builder.create();
    }

    @OnClick(R.id.btCamara)
    public void Camara()
    {

        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, 0);
        //SelectImageResourceDialog.this.getDialog().cancel();
    }

    @OnClick(R.id.btGagllery)
    public void Galeria()
    {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);
        //SelectImageResourceDialog.this.getDialog().cancel();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){

                    Bitmap photo = (Bitmap) imageReturnedIntent.getExtras().get("data");


                    listener.onGetImage(photo);
                    SelectImageResourceDialog.this.getDialog().cancel();

                }

                break;
            case 1:
                if(resultCode == RESULT_OK){

                    Uri selectedImage = imageReturnedIntent.getData();
                    Bitmap  mBitmap = null;
                    try {
                        mBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                    listener.onGetImage(mBitmap);
                    SelectImageResourceDialog.this.getDialog().cancel();
                }
                break;
        }
    }

}
