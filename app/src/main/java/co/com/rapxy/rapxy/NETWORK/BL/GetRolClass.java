package co.com.rapxy.rapxy.NETWORK.BL;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import co.com.rapxy.rapxy.MODAL.RolObject;
import co.com.rapxy.rapxy.NETWORK.WS.GetObjectClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.FIREURL_ROL;


/**
 * Created by Pedro on 22/01/2018.
 */

public class GetRolClass {
    public interface onGetRols
    {
        void onGetRols(List<RolObject> rolObjectList);
    }

    public void GetListRol(final onGetRols listener)
    {
        new GetObjectClass().getSimpleObjectsforAllChield(FIREURL_ROL, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {

                ArrayList<RolObject> rolObjects = new ArrayList<>();

                for(DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    RolObject rolObject = snapshot1.getValue(RolObject.class);
                    rolObject.setId(snapshot1.getKey());
                    rolObjects.add(rolObject);
                }

                if (rolObjects.size() >= 1)
                {
                    listener.onGetRols(rolObjects);
                }
                else
                {

                }
            }

            @Override
            public void onGetNullObject() {

            }

            @Override
            public void OnGetError(String Err) {

            }
        });
    }
}
