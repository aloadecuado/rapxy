package co.com.rapxy.rapxy.MODAL;

/**
 * Created by pedrodaza on 30/01/18.
 */

public class PhotosObject {

    private String id = "";
    private String urlPhoto = "";
    private String dateRegister = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    private Boolean activo = true;
}
