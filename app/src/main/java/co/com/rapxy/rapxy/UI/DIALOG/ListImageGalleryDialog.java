package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ADAPTER.DocumentAdpater;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

/**
 * Created by pedrodaza on 6/02/18.
 */
@SuppressLint("ValidFragment")
public class ListImageGalleryDialog extends DialogFragment {

    List<String> urlsImage;
    Context context;
    @SuppressLint("ValidFragment")
    public ListImageGalleryDialog(List<String> urlsImage, Context context)
    {
        this.urlsImage = urlsImage;
        this.context = context;
    }

    @BindView(R.id.my_recycler_view)
    RecyclerView recyclerView;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_list_image, null);

        ButterKnife.bind(this, view);


        LinearLayoutManager layoutManager = new LinearLayoutManager(context);

        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);

        String[] strarray = urlsImage.toArray(new String[0]);
        DocumentAdpater documentAdpater = new DocumentAdpater(strarray, context, new DocumentAdpater.onGetClick() {
            @Override
            public void onGetUrlImage(String urlImage) {

            }
        });

        recyclerView.setAdapter(documentAdpater);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view).setPositiveButton(R.string.btn_Ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {


                ListImageGalleryDialog.this.getDialog().cancel();


            }
        });
        return builder.create();
    }


}
