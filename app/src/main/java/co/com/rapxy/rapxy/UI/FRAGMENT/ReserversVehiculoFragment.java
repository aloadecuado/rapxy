package co.com.rapxy.rapxy.UI.FRAGMENT;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTabHost;

import co.com.rapxy.rapxy.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReserversVehiculoFragment extends Fragment {


    public ReserversVehiculoFragment() {
        // Required empty public constructor
    }

    private FragmentTabHost mTabHost;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        mTabHost = new FragmentTabHost(getActivity());
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_reservers_vehiculo);

        Bundle arg1 = new Bundle();
        arg1.putInt("Arg for Frag1", 1);
        mTabHost.addTab(mTabHost.newTabSpec("Tab1").setIndicator("", getResources().getDrawable(R.drawable.ic_drive_eta_black_24dp)),
                Reservers1VehiculoFragment.class, arg1);

        Bundle arg2 = new Bundle();
        arg2.putInt("Arg for Frag2", 2);
        mTabHost.addTab(mTabHost.newTabSpec("Tab2").setIndicator("", getResources().getDrawable(R.drawable.ic_domain_black_24dp)),
                Reservers2HotelFragment.class, arg2);
        // Inflate the layout for this fragment
        return mTabHost;
    }

}
