package co.com.rapxy.rapxy.UI.ACTIVITY;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.GetServicesClass;
import co.com.rapxy.rapxy.NETWORK.BL.GetUserClass;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.NETWORK.CUSTOMSERVICES.InmediateTravelServices;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ADAPTER.LocationAdapter;
import co.com.rapxy.rapxy.UI.ADAPTER.ServicesAdapter;
import co.com.rapxy.rapxy.UI.DIALOG.DetailMapLocationDialog;
import co.com.rapxy.rapxy.UI.DIALOG.PayTravelDialog;
import co.com.rapxy.rapxy.UI.DIALOG.QualifyDialog;
import co.com.rapxy.rapxy.UI.DIALOG.TravelVehiculoDialog;
import co.com.rapxy.rapxy.UI.FRAGMENT.ListHistorysFragment;
import co.com.rapxy.rapxy.UI.FRAGMENT.ListVehiculosFragment;
import co.com.rapxy.rapxy.UI.FRAGMENT.ReserversVehiculoFragment;
import co.com.rapxy.rapxy.UI.FRAGMENT.VehiculoUserFragment;
import co.com.rapxy.rapxy.UTIL.AlertsClass;
import co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.*;
public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, InmediateTravelServices.OnGetServices {


    MenuItem navConnect;
    @BindView(R.id.btService)
    FloatingActionButton btService;


    @BindView(R.id.lvServices)
    ListView lvServices;

    TextView tvUsuario;


    TextView tvNick;

    List<ServicesObject> SERVICESOBJECTS;

    public InmediateTravelServices inmediateTravelServices;

    private static final int REQUEST_RECORD_CAMARE_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();


        // find MenuItem you want to change


        navConnect = menu.findItem(R.id.navConnect);
        ButterKnife.bind(this);

        ActiveDesactive(USEROBJECT);
        ListVehiculosFragment listVehiculosFragment = new ListVehiculosFragment();
        setFragment(ListVehiculosFragment.class);

        tvUsuario = headerView.findViewById(R.id.tvUsuario);
        tvNick = headerView.findViewById(R.id.tvNick);
        if (USEROBJECT.getRol() != null)
        {
            tvUsuario.setText(USEROBJECT.getRol().getDescripcion());
        }

        tvNick.setText(USEROBJECT.getNick());

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_CAMARE_PERMISSION);

        inmediateTravelServices = new InmediateTravelServices(this);
        inmediateTravelServices.suscribeServices(this, USEROBJECT);

        lvServices.setVisibility(View.INVISIBLE);
    }

    public void suscribateServices(){
        inmediateTravelServices.suscribeServices(this, USEROBJECT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            /*case REQUEST_RECORD_CAMARE_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;*/
        }
        if (!permissionToRecordAccepted ) {
            //new AlertsClass().showAlerInfo(this, getResources().getString(R.string.alert_title_information), getResources().getString(R.string.camare_necesari));
        }

    }


    boolean bRelative = false;

    @OnClick(R.id.btService)
    public void ShowService()
    {


        if (!bRelative)
        {
            if(SERVICESOBJECTS == null){
                Toast.makeText(this,"No tiene servicios en este momento", Toast.LENGTH_LONG);
                return;
            }

            if(SERVICESOBJECTS.size() <= 0){
                Toast.makeText(this,"No tiene servicios en este momento", Toast.LENGTH_LONG);
                return;
            }
            lvServices.setVisibility(View.VISIBLE);
            bRelative = true;
        }
        else
        {
            lvServices.setVisibility(View.INVISIBLE);
            bRelative = false;
        }


        /*FragmentManager fragmentManager1 = getSupportFragmentManager();
        PayTravelDialog payTravelDialog = new PayTravelDialog(SERVICEOBJECT, USEROBJECT, new PayTravelDialog.onGetService() {
            @Override
            public void onGetService(ServicesObject servicesObject) {
                SERVICEOBJECT = servicesObject;
            }
        });

        payTravelDialog.show(fragmentManager1, "PayTravelDialog");
        FragmentManager fragmentManager = getSupportFragmentManager();
        TravelVehiculoDialog travelVehiculoDialog = new TravelVehiculoDialog(SERVICEOBJECT, USEROBJECT, USEROBJECTSERVICE, new TravelVehiculoDialog.onGetUserAddresVehiculo() {
            @Override
            public void onGetUser(ServicesObject servicesObject) {



            }

            @Override
            public void onGetPayService(ServicesObject servicesObject) {
                SERVICEOBJECT = servicesObject;
                FragmentManager fragmentManager1 = getSupportFragmentManager();
                PayTravelDialog payTravelDialog = new PayTravelDialog(SERVICEOBJECT, USEROBJECT, new PayTravelDialog.onGetService() {
                    @Override
                    public void onGetService(ServicesObject servicesObject) {
                        SERVICEOBJECT = servicesObject;
                    }
                });

                payTravelDialog.show(fragmentManager1, "PayTravelDialog");

            }
        });
        travelVehiculoDialog.show(fragmentManager, "DetailTRavel");*/
    }

    public void ActiveDesactive(UserObject userObject)
    {
        if (userObject.getState() == null) {

            navConnect.setTitle(getString(R.string.menu_name_disconnect));

            navConnect.setIcon(R.drawable.ic_radio_button_unchecked_black_24dp);
        }
        else if (userObject.getState().getValue().equals("AS"))
        {
            navConnect.setTitle(getString(R.string.menu_name_connect));

            navConnect.setIcon(R.drawable.ic_radio_button_checked_black_24dp);
        }
        else
        {
            navConnect.setTitle(getString(R.string.menu_name_disconnect));

            navConnect.setIcon(R.drawable.ic_radio_button_unchecked_black_24dp);
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.navUser) {

            setFragment(VehiculoUserFragment.class);
            // Handle the camera action
        }
        else if (id == R.id.navListVehiculos)
        {
            setFragment(ListVehiculosFragment.class);
        }
        else if (id == R.id.navReservers)
        {
            setFragment(ReserversVehiculoFragment.class);
        }
        else if (id == R.id.navHistory)
        {
            setFragment(ListHistorysFragment.class);
        }
        else if (id == R.id.navConnect)
        {


            new SetUserClass().updateUserState(this, USEROBJECT, STATES, new SetUserClass.onGetUser() {
                @Override
                public void onGetUser(UserObject userObject) {
                    USEROBJECT = userObject;
                    ActiveDesactive(USEROBJECT);
                }
            });

        } else if (id == R.id.navCloseSesion) {

            FirebaseAuth auth = FirebaseAuth.getInstance();
            auth.signOut();

            startActivity(new Intent(MenuActivity.this, MainActivity.class));
            MenuActivity.this.finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setFragment(Class fragmentClass)
    {
        Fragment fragment = null;


        fragmentClass = fragmentClass;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

    }

    private void setAdapterServices(List<ServicesObject> servicesObjects){
        SERVICESOBJECTS = servicesObjects;
        ServicesAdapter servicesAdapter = new ServicesAdapter(this, R.layout.adapter_services, servicesObjects);
        lvServices.setAdapter(servicesAdapter);

        lvServices.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    final int position, long id) {



                new GetUserClass().getUserForId(MenuActivity.this, SERVICESOBJECTS.get(position).getIdUser(), new GetUserClass.onGetUser() {
                    @Override
                    public void onGetUser(final UserObject userObjectsCliente) {
                        new GetUserClass().getUserForId(MenuActivity.this, SERVICESOBJECTS.get(position).getIdDriver(), new GetUserClass.onGetUser() {
                            @Override
                            public void onGetUser(UserObject userObjectsVehiculo) {

                                if(SERVICESOBJECTS.get(position).getState().getValue().equals("FS")){
                                    FragmentManager fragmentManager1 = getSupportFragmentManager();
                                    QualifyDialog payTravelDialog = new QualifyDialog(MenuActivity.this, SERVICESOBJECTS.get(position), USEROBJECT, new QualifyDialog.onGetService() {
                                        @Override
                                        public void onGetService(ServicesObject servicesObject) {
                                            SERVICEOBJECT = servicesObject;
                                        }
                                    });

                                    payTravelDialog.show(fragmentManager1, "QualifyDialog");
                                }else{

                                    FragmentManager fragmentManager = getSupportFragmentManager();
                                    TravelVehiculoDialog travelUserDialog = new TravelVehiculoDialog(SERVICESOBJECTS.get(position), USEROBJECT,userObjectsCliente, userObjectsVehiculo, new TravelVehiculoDialog.onGetUserAddresVehiculo() {
                                        @Override
                                        public void onGetUser(ServicesObject servicesObject) {
                                            //SERVICEOBJECT = servicesObject;
                                        }

                                        @Override
                                        public void onGetPayService(ServicesObject servicesObject) {

                                            FragmentManager fragmentManager1 = getSupportFragmentManager();
                                            PayTravelDialog payTravelDialog = new PayTravelDialog(servicesObject, USEROBJECT, new PayTravelDialog.onGetService() {
                                                @Override
                                                public void onGetService(ServicesObject servicesObject) {
                                                    SERVICEOBJECT = servicesObject;
                                                }
                                            });

                                            payTravelDialog.show(fragmentManager1, "PayTravelDialog");


                                        }
                                    });
                                    travelUserDialog.show(fragmentManager, "TravelUserDialog");
                                }

                            }
                        });
                    }
                });
                //inmediateTravelServices.suscribeServices(MenuActivity.this, USEROBJECT);

            }

        });
    }
    @Override
    public void onServices(List<ServicesObject> servicesObjects) {
        setAdapterServices(servicesObjects);
    }

    private void showDialohTravel(ServicesObject servicesObject){
        SERVICEOBJECT = servicesObject;
        new GetUserClass().getUserForId(this, servicesObject.getIdUser(), new GetUserClass.onGetUser() {
            @Override
            public void onGetUser(final UserObject userObjectsCliente) {
                new GetUserClass().getUserForId(MenuActivity.this, servicesObject.getIdDriver(), new GetUserClass.onGetUser() {
                    @Override
                    public void onGetUser(UserObject userObjectsVehiculo) {

                        FragmentManager fragmentManager = getSupportFragmentManager();
                        TravelVehiculoDialog travelUserDialog = new TravelVehiculoDialog(SERVICEOBJECT, USEROBJECT,userObjectsCliente, userObjectsVehiculo, new TravelVehiculoDialog.onGetUserAddresVehiculo() {
                            @Override
                            public void onGetUser(ServicesObject servicesObject) {
                                //SERVICEOBJECT = servicesObject;


                            }

                            @Override
                            public void onGetPayService(ServicesObject servicesObject) {

                                FragmentManager fragmentManager1 = getSupportFragmentManager();
                                PayTravelDialog payTravelDialog = new PayTravelDialog(servicesObject, USEROBJECT, new PayTravelDialog.onGetService() {
                                    @Override
                                    public void onGetService(ServicesObject servicesObject) {
                                        SERVICEOBJECT = servicesObject;
                                    }
                                });

                                payTravelDialog.show(fragmentManager1, "PayTravelDialog");




                            }
                        });
                        travelUserDialog.show(fragmentManager, "TravelUserDialog");
                    }
                });
            }
        });
    }
    @Override
    public void onPeddingServices(ServicesObject servicesObject) {



        showDialohTravel(servicesObject);



    }

    @Override
    public void onCounterProposalService(ServicesObject servicesObject) {
        showDialohTravel(servicesObject);
    }


    @Override
    public void onAcepptedServices(ServicesObject servicesObject) {
        showDialohTravel(servicesObject);

    }

    @Override
    public void onArrivedServices(ServicesObject servicesObject) {
        showDialohTravel(servicesObject);

    }

    @Override
    public void onFinishServices(ServicesObject servicesObject) {
        SERVICEOBJECT = servicesObject;

    }


    @Override
    public void onQualifiedServices(ServicesObject servicesObject) {
        SERVICEOBJECT = servicesObject;

    }

    @Override
    public void onPayProcecedServices(ServicesObject servicesObject) {
        SERVICEOBJECT = servicesObject;

    }

    @Override
    public void onCancelServices(ServicesObject servicesObject) {
        SERVICEOBJECT = servicesObject;

        inmediateTravelServices.suscribeServices(this, USEROBJECT);

    }

    /*
    *     private void setServicesAdapter()
    {
        new GetServicesClass().getAllSercices(this, USEROBJECT, new GetServicesClass.onGetServicesUser() {
            @Override
            public void onGetServiceUser(List<ServicesObject> servicesObject) {


            }

            @Override
            public void onNoService() {

            }
        });
    }*/
}
