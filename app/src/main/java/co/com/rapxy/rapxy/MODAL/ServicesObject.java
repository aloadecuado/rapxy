package co.com.rapxy.rapxy.MODAL;

/**
 * Created by pedrodaza on 7/02/18.
 */

public class ServicesObject {

    private String id = "";
    private String idUser = "";
    private String idDriver = "";
    private AddresObject address = null;

    public PlanObject getPlan() {
        return plan;
    }

    public void setPlan(PlanObject plan) {
        this.plan = plan;
    }

    private PlanObject plan = null;
    private ListObjetc type = null;
    private Double lat = 0.0;
    private Double lot = 0.0;
    private StateObjetc state = null;

    public PayServiceObject getPayService() {
        return payService;
    }

    public void setPayService(PayServiceObject payService) {
        this.payService = payService;
    }

    private PayServiceObject payService = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public AddresObject getAddress() {
        return address;
    }

    public void setAddress(AddresObject address) {
        this.address = address;
    }

    public ListObjetc getType() {
        return type;
    }

    public void setType(ListObjetc type) {
        this.type = type;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLot() {
        return lot;
    }

    public void setLot(Double lot) {
        this.lot = lot;
    }

    public StateObjetc getState() {
        return state;
    }

    public void setState(StateObjetc state) {
        this.state = state;
    }

    public long getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(long dateRegister) {
        this.dateRegister = dateRegister;
    }

    public long getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(long dateReservation) {
        this.dateReservation = dateReservation;
    }

    private long dateRegister = 0;
    private String dateRegisterString = "";
    private long dateReservation = 0;

    public String getDateRegisterString() {
        return dateRegisterString;
    }

    public void setDateRegisterString(String dateRegisterString) {
        this.dateRegisterString = dateRegisterString;
    }

    private QualifyObject qualifyObjectClient = null;

    public QualifyObject getQualifyObjectClient() {
        return qualifyObjectClient;
    }

    public void setQualifyObjectClient(QualifyObject qualifyObjectClient) {
        this.qualifyObjectClient = qualifyObjectClient;
    }

    public QualifyObject getQualifyObjectVehiculo() {
        return qualifyObjectVehiculo;
    }

    public void setQualifyObjectVehiculo(QualifyObject qualifyObjectVehiculo) {
        this.qualifyObjectVehiculo = qualifyObjectVehiculo;
    }

    private QualifyObject qualifyObjectVehiculo = null;
}
