package co.com.rapxy.rapxy.UI.ADAPTER;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import java.util.List;

import co.com.rapxy.rapxy.MODAL.CreditCardObject;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.R;

/**
 * Created by pedrodaza on 15/02/18.
 */

public class CreditCardAdapter extends ArrayAdapter<CreditCardObject> {

    Context context;
    public CreditCardAdapter(@NonNull Context mcontext, @LayoutRes int resource, List<CreditCardObject> creditCardObjects)
    {
        super(mcontext,resource, creditCardObjects);

        context = mcontext;

    }




    @Override
    public View getView(int groupPosition, View convertView, ViewGroup parent) {

        Context con = null;
        View view = convertView;
        if (view == null)
        {
            //  LayoutInflater vi = (LayoutInflater)con.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LayoutInflater vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.adapter_credit_card, null);
        }

        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        TextView tvNumber = (TextView) view.findViewById(R.id.tvNumber);
        TextView tvDateVencimiento = (TextView) view.findViewById(R.id.tvDateVencimiento);



        CreditCardObject creditCardObject = getItem(groupPosition);

        String finalNumber = creditCardObject.getCreditNumber().substring((creditCardObject.getCreditTitualarName().length() - 5), creditCardObject.getCreditTitualarName().length() - 1);
        tvName.setText("Nombre: " +  creditCardObject.getCreditTitualarName());
        tvNumber.setText("Numero: **** **** **** " + finalNumber);
        tvDateVencimiento.setText("Fecha de Vencimiento: "+ creditCardObject.getDateExpedition());



        return view;
    }
}
