package co.com.rapxy.rapxy.UI.FRAGMENT;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import co.com.rapxy.rapxy.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Reservers2HotelFragment extends Fragment {


    public Reservers2HotelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reservers2_hotel, container, false);
        // Inflate the layout for this fragment
        return view;
    }

}
