package co.com.rapxy.rapxy.MODAL;

/**
 * Created by pedrodaza on 12/02/18.
 */

public class PagoNequiObject {
    private String nameUserAcount = "";

    public String getNameUserAcount() {
        return nameUserAcount;
    }

    public void setNameUserAcount(String nameUserAcount) {
        this.nameUserAcount = nameUserAcount;
    }

    public String getNumberCellNequi() {
        return numberCellNequi;
    }

    public void setNumberCellNequi(String numberCellNequi) {
        this.numberCellNequi = numberCellNequi;
    }
    private String numberCellNequi = "";

    public String getUrlComprovante() {
        return urlComprovante;
    }

    public void setUrlComprovante(String urlComprovante) {
        this.urlComprovante = urlComprovante;
    }

    private String urlComprovante = "";
}
