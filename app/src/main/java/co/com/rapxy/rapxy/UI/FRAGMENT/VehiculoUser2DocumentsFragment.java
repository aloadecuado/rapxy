package co.com.rapxy.rapxy.UI.FRAGMENT;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ACTIVITY.MainActivity;
import co.com.rapxy.rapxy.UI.ACTIVITY.MenuActivity;
import co.com.rapxy.rapxy.UI.ADAPTER.DocumentAdpater;
import co.com.rapxy.rapxy.UI.DIALOG.DetailImageDialog;
import co.com.rapxy.rapxy.UI.DIALOG.SelectAppDialog;
import co.com.rapxy.rapxy.UI.DIALOG.SelectImageResourceDialog;
import co.com.rapxy.rapxy.UTIL.CallStorageResourceClass;
import co.com.rapxy.rapxy.UTIL.WriteReadStorageClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehiculoUser2DocumentsFragment extends Fragment {


    public VehiculoUser2DocumentsFragment() {
        // Required empty public constructor
    }

    private String [] permissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final int REQUEST_RECORD_CAMARE_PERMISSION = 200;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_vehiculo_user2_documents, container, false);
        // Inflate the layout for this fragment

        ButterKnife.bind(this, view);

        ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_RECORD_CAMARE_PERMISSION);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);


        // specify an adapter (see also next example)
        setAdapter();

        return view;
    }

    private void setAdapter()
    {
        if (USEROBJECT.getUrlsDocuments() != null)
        {
            String[] Documents = new String[USEROBJECT.getUrlsDocuments().size()];
            int i = 0;
            for (String Description : USEROBJECT.getUrlsDocuments())
            {
                Documents[i] = Description;

                i++;
            }

            mAdapter = new DocumentAdpater(Documents, getActivity(), new DocumentAdpater.onGetClick() {
                @Override
                public void onGetUrlImage(String urlImage) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    DetailImageDialog editNameDialogFragment = new DetailImageDialog(getContext(), urlImage, USEROBJECT, true, new DetailImageDialog.onGetUser() {
                        @Override
                        public void onGetUser(UserObject userObject) {
                            USEROBJECT = userObject;
                            setAdapter();

                        }
                    });
                    editNameDialogFragment.show(fm, "fragment_edit_name");
                }
            });
            mRecyclerView.setAdapter(mAdapter);
        }
    }
    private boolean permissionToRecordAccepted = false;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_CAMARE_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) getActivity().finish();

    }

    @OnClick(R.id.btAdd)
    public void Add()
    {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        SelectImageResourceDialog editNameDialogFragment = new SelectImageResourceDialog(new SelectImageResourceDialog.onGetImage() {
            @Override
            public void onGetImage(Bitmap bitmap) {
                Bitmap uri1 = bitmap;

                new SetUserClass().addBitmapUserForId(bitmap, USEROBJECT, new SetUserClass.onGetUser() {
                    @Override
                    public void onGetUser(UserObject userObject) {
                        USEROBJECT = userObject;

                        setAdapter();
                    }
                });
            }
        });
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }



}
