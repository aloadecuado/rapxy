package co.com.rapxy.rapxy.MODAL;

/**
 * Created by pedrodaza on 12/02/18.
 */

public class CreditCardObject {

    private String creditNumber = "";
    private String creditTitualarName = "";
    private String dateExpedition = "";
    private String codeDeVinculacion = "";

    public String getCreditNumber() {
        return creditNumber;
    }

    public void setCreditNumber(String creditNumber) {
        this.creditNumber = creditNumber;
    }

    public String getCreditTitualarName() {
        return creditTitualarName;
    }

    public void setCreditTitualarName(String creditTitualarName) {
        this.creditTitualarName = creditTitualarName;
    }

    public String getDateExpedition() {
        return dateExpedition;
    }

    public void setDateExpedition(String dateExpedition) {
        this.dateExpedition = dateExpedition;
    }

    public String getCodeDeVinculacion() {
        return codeDeVinculacion;
    }

    public void setCodeDeVinculacion(String codeDeVinculacion) {
        this.codeDeVinculacion = codeDeVinculacion;
    }


}
