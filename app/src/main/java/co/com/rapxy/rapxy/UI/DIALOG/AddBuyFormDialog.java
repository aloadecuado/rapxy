package co.com.rapxy.rapxy.UI.DIALOG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.fragment.app.DialogFragment;

import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.com.rapxy.rapxy.MODAL.CreditCardObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.ValidationClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;

/**
 * Created by pedrodaza on 13/02/18.
 */
@SuppressLint("ValidFragment")
public class AddBuyFormDialog  extends DialogFragment {

    public interface onGetUser
    {
        void onGetUser(UserObject userObject);
    }

    Context context; onGetUser listener;UserObject userObject;
    @SuppressLint("ValidFragment")
    public AddBuyFormDialog(Context context, UserObject userObject, onGetUser listener)
    {

        this.context = context;
        this.listener = listener;
        this.userObject = userObject;
    }

    @BindView(R.id.lyGeneral)
    LinearLayout lyGeneral;
    @BindView(R.id.etNumberCard)
    EditText etNumberCard;

    @BindView(R.id.etNameCard)
    EditText etNameCard;

    @BindView(R.id.etSecureteNumber)
    EditText etSecureteNumber;

    @BindView(R.id.spMountCard)
    MaterialSpinner spMountCard;

    @BindView(R.id.spYearCard)
    MaterialSpinner spYearCard;

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_add_buy_form, null);
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        String[] aStringMounth = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        String[] aStringYear = {"2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029"};

        spMountCard.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, aStringMounth));
        spYearCard.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, aStringYear));


        builder.setView(view)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                if (new ValidationClass().ValidacionCamposVacios(context, lyGeneral))
                {
                    CreditCardObject creditCardObject = new CreditCardObject();

                    String fechaExpedition = spMountCard.getText().toString() + "/" + spYearCard.getText().toString();

                    creditCardObject.setCreditNumber(etNumberCard.getText().toString());
                    creditCardObject.setCreditTitualarName(etNameCard.getText().toString());
                    creditCardObject.setDateExpedition(fechaExpedition);
                    creditCardObject.setCodeDeVinculacion(etSecureteNumber.getText().toString());


                    new SetUserClass().addCreditCardUserForId(context, USEROBJECT, creditCardObject, new SetUserClass.onGetUser() {
                        @Override
                        public void onGetUser(UserObject userObject) {

                            listener.onGetUser(userObject);
                        }
                    });
                }






            }
        })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        AddBuyFormDialog.this.getDialog().cancel();

                    }
                });
        return builder.create();
    }
}
