package co.com.rapxy.rapxy.UI.FRAGMENT;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTabHost;

import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehiculoUserFragment extends Fragment {


    public VehiculoUserFragment() {
        // Required empty public constructor
    }


    private FragmentTabHost mTabHost;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mTabHost = new FragmentTabHost(getActivity());
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_vehiculo_user);

        Bundle arg1 = new Bundle();
        arg1.putInt("Arg for Frag1", 1);
        mTabHost.addTab(mTabHost.newTabSpec("Tab1").setIndicator("", getResources().getDrawable(R.drawable.ic_person_black_24dp)),
                VehiculoUser1DataBasicFragment.class, arg1);

        Bundle arg2 = new Bundle();
        arg2.putInt("Arg for Frag2", 2);
        mTabHost.addTab(mTabHost.newTabSpec("Tab2").setIndicator("", getResources().getDrawable(R.drawable.ic_insert_drive_file_black_24dp)),
                VehiculoUser2DocumentsFragment.class, arg2);

        Bundle arg3 = new Bundle();
        arg3.putInt("Arg for Frag3", 3);
        mTabHost.addTab(mTabHost.newTabSpec("Tab3").setIndicator("", getResources().getDrawable(R.drawable.ic_insert_photo_black_24dp)),
                VehiculoUser3PhotosFragment.class, arg3);

        Bundle arg4 = new Bundle();
        arg4.putInt("Arg for Frag4", 4);
        mTabHost.addTab(mTabHost.newTabSpec("Tab4").setIndicator("", getResources().getDrawable(R.drawable.ic_room_black_24dp)),
                VehiculoUser4LocationFragment.class, arg4);

        Bundle arg5 = new Bundle();
        arg5.putInt("Arg for Frag5", 5);
        mTabHost.addTab(mTabHost.newTabSpec("Tab5").setIndicator("", getResources().getDrawable(R.drawable.ic_assignment_black_24dp)),
                VehiculoUser5PlansFragment.class, arg5);

        Bundle arg6 = new Bundle();
        arg5.putInt("Arg for Frag6", 6);
        mTabHost.addTab(mTabHost.newTabSpec("Tab6").setIndicator("", getResources().getDrawable(R.drawable.ic_credit_card_black_24dp)),
                VehiculoUser6CreditCardFragment.class, arg6);

        return mTabHost;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTabHost = null;
    }


}
