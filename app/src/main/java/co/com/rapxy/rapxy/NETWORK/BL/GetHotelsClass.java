package co.com.rapxy.rapxy.NETWORK.BL;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.NETWORK.WS.GetObjectClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.FIREURL_HOTEL_ADRESS;

/**
 * Created by pedrodaza on 7/02/18.
 */

public class GetHotelsClass {

    public interface onGetHotels
    {
        void onGetHotels(List<AddresObject> hotelsObject);
    }

    public void getHotels (final onGetHotels listener)
    {
        new GetObjectClass().getSimpleListObjectUidKey(FIREURL_HOTEL_ADRESS, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                List<AddresObject> hotelsObject = new ArrayList<>();

                for (DataSnapshot snapshot1 :snapshot.getChildren())
                {
                    AddresObject addresObject = snapshot1.getValue(AddresObject.class);

                    hotelsObject.add(addresObject);
                }

                if (hotelsObject.size() >= 1)
                {
                    listener.onGetHotels(hotelsObject);
                }
            }

            @Override
            public void onGetNullObject() {

            }

            @Override
            public void OnGetError(String Err) {

            }
        });
    }
}
