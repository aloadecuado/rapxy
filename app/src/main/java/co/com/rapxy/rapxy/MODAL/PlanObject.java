package co.com.rapxy.rapxy.MODAL;

/**
 * Created by pedrodaza on 8/02/18.
 */

public class PlanObject {
    private String description = "";
    private String id = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumberHour() {
        return numberHour;
    }

    public void setNumberHour(int numberHour) {
        this.numberHour = numberHour;
    }

    public double getPercentageDescount() {
        return percentageDescount;
    }

    public void setPercentageDescount(double percentageDescount) {
        this.percentageDescount = percentageDescount;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public StateObjetc getState() {
        return state;
    }

    public void setState(StateObjetc state) {
        this.state = state;
    }

    public String getFormatPrice()
    {
        return "$" + price;
    }
    private String nombre = "";

    private int numberHour = 0;
    private double percentageDescount = 0.0;
    private int price = 0;
    private StateObjetc state = null;
}
