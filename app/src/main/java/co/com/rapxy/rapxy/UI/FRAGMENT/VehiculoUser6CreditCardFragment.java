package co.com.rapxy.rapxy.UI.FRAGMENT;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.CreditCardObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ADAPTER.CreditCardAdapter;
import co.com.rapxy.rapxy.UI.ADAPTER.PlanAdapter;
import co.com.rapxy.rapxy.UI.DIALOG.AddBuyFormDialog;
import co.com.rapxy.rapxy.UI.DIALOG.AddPlanDialog;
import co.com.rapxy.rapxy.UI.DIALOG.DetailPlanDialog;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehiculoUser6CreditCardFragment extends Fragment {


    public VehiculoUser6CreditCardFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.lvCreditCards)
    ListView lvCreditCards;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_vehiculo_user6_credit_card, container, false);
        ButterKnife.bind(this, view );
        SetAdapterList();
        // Inflate the layout for this fragment
        return view;
    }
    private void SetAdapterList()
    {

        if (USEROBJECT.getCreditCards() != null)
        {
            CreditCardAdapter creditCardAdapter = new CreditCardAdapter(getActivity(), R.layout.adapter_plan, USEROBJECT.getCreditCards());
            lvCreditCards.setAdapter(creditCardAdapter);
            lvCreditCards.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    final CreditCardObject creditCardObject = USEROBJECT.getCreditCards().get(position);
                    new AlertsClass().showAlerInfoYesOrNo(getContext(), getResources().getString(R.string.alert_title_information), getResources().getString(R.string.alert_message_erase), new AlertsClass.onGetData() {
                        @Override
                        public void onGetDataOk() {
                            new SetUserClass().removeCreditCardUserForId(getActivity(), USEROBJECT, creditCardObject, new SetUserClass.onGetUser() {
                                @Override
                                public void onGetUser(UserObject userObject) {
                                    USEROBJECT = userObject;
                                    SetAdapterList();
                                }
                            });
                        }

                        @Override
                        public void onGetDataCalcel() {

                        }
                    });
                }
            });
        }

    }
    @OnClick(R.id.btAdd)
    public void Add()
    {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        AddBuyFormDialog addPlanDialog = new AddBuyFormDialog(getActivity(), USEROBJECT, new AddBuyFormDialog.onGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {
                USEROBJECT = userObject;
                SetAdapterList();
            }
        });


        addPlanDialog.show(fm, "fragment_edit_name");

    }

}
