package co.com.rapxy.rapxy.UI.DIALOG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.R;
import co.com.seletiene.seletieneutil.CUSTOMCONTROLLERS.PedroEditText;

/**
 * Created by pedroalonsodazab on 4/03/18.
 */
@SuppressLint("ValidFragment")
public class AddReserverVehiculoDialog extends DialogFragment implements OnMapReadyCallback {

    @SuppressLint("ValidFragment")
    public AddReserverVehiculoDialog(List<AddresObject> Hotels, UserObject userObject, UserObject userObjectDrive, onGetUserAddresVehiculo onGetUser)
    {
        this.Hotels = Hotels;
        this.userObject = userObject;
        this.userObjectDrive = userObjectDrive;
        listener = onGetUser;
    }
    List<AddresObject> Hotels;
    UserObject userObject;
    UserObject userObjectDrive;
    List<AddresObject> addresObjects;
    AddresObject ADDREESSSSELECT;
    PlanObject PLANOBJECT;

    String RESERVERDATE = "";
    String RESERVEDTIME = "";
    onGetUserAddresVehiculo listener;
    public interface onGetUserAddresVehiculo
    {

        void onGetUser(AddresObject addresObject, UserObject userObject, UserObject userObjectVehiculo, PlanObject planObject);

    }


    @BindView(R.id.map)
    MapView mapView;
    GoogleMap map;

    @BindView(R.id.tabHost)
    TabHost tabHost;

    @BindView(R.id.spLocation)
    Spinner spLocation;

    @BindView(R.id.spPlan)
    Spinner spPlan;

    @BindView(R.id.etFecha)
    PedroEditText etFecha;

    @BindView(R.id.etTime)
    PedroEditText etTime;

    Calendar myCalendar = Calendar.getInstance();
    private static final int REQUEST_RECORD_CAMARE_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_add_reservation, null);
        ButterKnife.bind(this, view);

        ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_RECORD_CAMARE_PERMISSION);
        tabHost.setup();


        TabHost.TabSpec tabpage2 = tabHost.newTabSpec("two");
        tabpage2.setContent(R.id.tab2);
        tabpage2.setIndicator("", getResources().getDrawable(R.drawable.ic_person_black_24dp));

        TabHost.TabSpec tabpage3 = tabHost.newTabSpec("tree");
        tabpage3.setContent(R.id.tab3);
        tabpage3.setIndicator("", getResources().getDrawable(R.drawable.ic_drive_eta_black_24dp));

        tabHost.addTab(tabpage2);
        tabHost.addTab(tabpage3);

        final String[] asTipoIds = getArrayStringAdress(userObject.getAddres());
        final String[] asPlans = getArrayStringPlans(userObjectDrive.getPlan());

        spLocation.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, asTipoIds));
        spPlan.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, asPlans));

        if (userObject.getAddres() != null && userObject.getAddres().size() >= 1) {
            addresObjects = userObject.getAddres();

        }
        if (userObjectDrive.getPlan() != null && userObjectDrive.getPlan().size() >= 1)
        {
            PLANOBJECT = userObjectDrive.getPlan().get(0);
        }

        etFecha.setFocusableInTouchMode(true);
        etFecha.requestFocus();
        etFecha.setEnabled(false);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

            }

        };

        etFecha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b)
                {
                    new DatePickerDialog(getActivity(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });

        etFecha.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });


        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {

                if (tabId.equals("two") )
                {
                    String[] asTipoIds = getArrayStringAdress(userObject.getAddres());
                    if (userObject.getAddres() != null && userObject.getAddres().size() >= 1) {
                        addresObjects = userObject.getAddres();
                    }

                    spLocation.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, asTipoIds));
                }
                else if (tabId.equals("tree") )
                {
                    String[] asTipoIds = getArrayStringAdress(userObjectDrive.getAddres());
                    if (userObjectDrive.getAddres() != null && userObjectDrive.getAddres().size() >= 1) {
                        addresObjects = userObjectDrive.getAddres();
                    }

                    spLocation.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, asTipoIds));
                }

            }
        });
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(this);

        spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ADDREESSSSELECT = addresObjects.get(position);

                setLocation(new LatLng(ADDREESSSSELECT.getLat(), ADDREESSSSELECT.getLog()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PLANOBJECT = userObjectDrive.getPlan().get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        builder.setView(view)
                .setPositiveButton(R.string.btn_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {


                        listener.onGetUser(ADDREESSSSELECT, userObject, userObjectDrive, PLANOBJECT);




                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AddReserverVehiculoDialog.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etFecha.setText(sdf.format(myCalendar.getTime()));
    }


    private String[] getArrayStringAdress(List<AddresObject> addresObjects)
    {
        if (addresObjects != null) {
            String[] sAdreess = new String[addresObjects.size()];
            int i = 0;
            for (AddresObject addresObject : addresObjects) {
                sAdreess[i] = addresObject.getName();
                i++;
            }
            return sAdreess;
        }
        else
        {
            String[] sAdreess = {""};
            return sAdreess;
        }



    }

    private String[] getArrayStringPlans(List<PlanObject> planObjects)
    {
        if (planObjects != null) {
            String[] splans = new String[planObjects.size()];
            int i = 0;
            for (PlanObject planObject : planObjects) {
                splans[i] = "Plan: " + planObject.getDescription() + "\nPrecio: " + planObject.getPrice();
                i++;
            }
            return splans;
        }
        else
        {
            String[] splans = {""};
            return splans;
        }



    }


    private PlanObject getPlanForDescription(String Sppiner){

        PlanObject planObject = new PlanObject();
        for (PlanObject plan : userObjectDrive.getPlan()){

            if (Sppiner.contains(plan.getDescription())){
                PLANOBJECT = plan;
                break;
            }
        }

        return PLANOBJECT;
    }

    @OnClick(R.id.btDate)
    public void selctedDateReservedPressed(View view){

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub


                RESERVERDATE = "" + year + "/" + returnDateFormat(monthOfYear + 1) + "/" + returnDateFormat(dayOfMonth);
                etFecha.setText(RESERVERDATE);


            }

        };

        new DatePickerDialog(getActivity(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.btHour)
    public void selctedHourReservedPressed(View view){


        final TimePickerDialog.OnTimeSetListener time = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {

                RESERVEDTIME = returnDateFormat(i) + ":" + returnDateFormat(i1) + ":" + "00";
                etTime.setText(RESERVEDTIME);

                String dateString = "03/26/2012 11:49:00 AM";
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
                Date convertedDate = new Date();
                try {
                    convertedDate = dateFormat.parse(RESERVERDATE + " " + RESERVEDTIME);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                System.out.println(convertedDate);
            }
        };

        new TimePickerDialog(getActivity(), time, myCalendar
                .get(Calendar.HOUR), myCalendar.get(Calendar.MINUTE), false).show();
    }
    public String returnDateFormat(int item){
        String dato = "" + item;
        if (dato.length() <= 1){
            return "0" + dato;
        }

        return dato;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;



        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {

            if (userObject.getAddres() != null && userObject.getAddres().size() >= 1) {
                ADDREESSSSELECT = userObject.getAddres().get(0);
                setLocation(new LatLng(userObject.getAddres().get(0).getLat(), userObject.getAddres().get(0).getLog()));
            }


        }
    }

    private void setLocation(LatLng latLng)
    {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {

            RemoveAllMarkets();
            addMarket(new LatLng(latLng.latitude, latLng.longitude), "Mi Destino");
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }

    private void RemoveAllMarkets()
    {
        for (Marker marker : markerOptionss)
        {
            marker.remove();
        }

        markerOptionss = new ArrayList<>();
    }

    List<Marker> markerOptionss = new ArrayList<>();
    private Marker addMarket(LatLng latLng, String title)
    {
        MarkerOptions markerOptions = new MarkerOptions();
        Marker marker = map.addMarker(markerOptions
                .position(latLng)
                .title(title));

        markerOptionss.add(marker);

        return marker;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_CAMARE_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (userObject.getAddres() != null && userObject.getAddres().size() >= 1) {
                    setLocation(new LatLng(userObject.getAddres().get(0).getLat(), userObject.getAddres().get(0).getLog()));
                }
                break;
        }
        if (!permissionToRecordAccepted) getActivity().finish();

    }
}
