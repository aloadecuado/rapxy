package co.com.rapxy.rapxy.UI.FRAGMENT;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiConfiguration;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;


import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.SetUserClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.ADAPTER.DocumentAdpater;
import co.com.rapxy.rapxy.UI.ADAPTER.LocationAdapter;
import co.com.rapxy.rapxy.UI.DIALOG.DetailMapLocationDialog;
import co.com.rapxy.rapxy.UI.DIALOG.ResumeAddLocationDialog;
import co.com.rapxy.rapxy.UI.DIALOG.SelectImageResourceDialog;

import static android.content.ContentValues.TAG;
import static android.content.Context.LOCATION_SERVICE;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehiculoUser4LocationFragment extends Fragment implements OnMapReadyCallback {


    public VehiculoUser4LocationFragment() {
        // Required empty public constructor
    }


    MapView mapView;
    GoogleMap map;

    private static final int REQUEST_RECORD_CAMARE_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    View view;


    @BindView(R.id.btAdd)
    FloatingActionButton btAdd;

    @BindView(R.id.lvLocations)
    ListView lvLocations;

    @BindView(R.id.rlListLocations)
    RelativeLayout rlListLocations;


    AddresObject ADDRESOBJECT;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null)
        {

// Gets the MapView from the XML layout and creates it

        }
        else
        {

            view = inflater.inflate(R.layout.fragment_vehiculo_user4_location, container, false);
// Gets the MapView from the XML layout and creates it

            ButterKnife.bind(this, view);

            btAdd.setVisibility(View.INVISIBLE);
            mapView = (MapView) view.findViewById(R.id.map);
            mapView.onCreate(savedInstanceState);

            ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_RECORD_CAMARE_PERMISSION);
            mapView.getMapAsync(this);

            setLocationAdapter();
            rlListLocations.setVisibility(View.INVISIBLE);

            // Initialize Places.
            if(!Places.isInitialized()){
                Places.initialize(getContext(), "AIzaSyADusaPg7IjGCvdPwqAbMah77TYjn3QI14");
            }


// Create a new Places client instance.

            List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS);

            AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);

            autocompleteFragment.setPlaceFields(placeFields);
            //autocompleteFragment = (PlaceAutocompleteFragment)getActivity().getFragmentManager().findFragmentById(R.id.autocomplete_fragment);

            PlacesClient placesClient = Places.createClient(getContext());


            FindCurrentPlaceRequest request =
                    FindCurrentPlaceRequest.newInstance(placeFields);

            Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
            placeResponse.addOnCompleteListener(task -> {
                if (task.isSuccessful()){
                    FindCurrentPlaceResponse response = task.getResult();
                    for (PlaceLikelihood placeLikelihood : response.getPlaceLikelihoods()) {
                        Log.i(TAG, String.format("Place '%s' has likelihood: %f",
                                placeLikelihood.getPlace().getName(),
                                placeLikelihood.getLikelihood()));

                        Place place = placeLikelihood.getPlace();
                        if (!place.getName().toString().equals(""))
                        {

                            ADDRESOBJECT = new AddresObject();
                            ADDRESOBJECT.setName(place.getName().toString());
                            ADDRESOBJECT.setAddres(place.getAddress().toString());
                            ADDRESOBJECT.setLat(place.getLatLng().latitude);
                            ADDRESOBJECT.setLog(place.getLatLng().longitude);
                            if (autocompleteFragment != null){
                                autocompleteFragment.setText(place.getAddress().toString());
                            }

                            btAdd.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    Exception exception = task.getException();
                    if (exception instanceof ApiException) {
                        ApiException apiException = (ApiException) exception;
                        Log.e(TAG, "Place not found: " + apiException.getStatusCode());
                    }
                }
            });

            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    // TODO: Get info about the selected place.
                    Log.i(TAG, "Place: " + place.getName());

                    setLocation(place.getLatLng());

                    RemoveAllMarkets();
                    addMarket(place.getLatLng(), place.getName().toString());

                    if (!place.getName().toString().equals(""))
                    {

                        ADDRESOBJECT = new AddresObject();
                        ADDRESOBJECT.setName(place.getName().toString());
                        ADDRESOBJECT.setAddres(place.getAddress().toString());
                        ADDRESOBJECT.setLat(place.getLatLng().latitude);
                        ADDRESOBJECT.setLog(place.getLatLng().longitude);
                        btAdd.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onError(Status status) {
                    // TODO: Handle the error.
                    Log.i(TAG, "An error occurred: " + status);
                }

            });

        }


        return view;
    }



    private void setLocationAdapter()
    {
        if (USEROBJECT.getAddres() != null) {
            LocationAdapter locationAdapter = new LocationAdapter(getActivity(), R.layout.adapter_location, USEROBJECT.getAddres());
            lvLocations.setAdapter(locationAdapter);
            lvLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        final int position, long id) {

                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    if (USEROBJECT.getAddres() != null)
                    {
                        DetailMapLocationDialog resumeAddLocationDialog = new DetailMapLocationDialog(USEROBJECT.getAddres().get(position), USEROBJECT, position, new DetailMapLocationDialog.onGetUser() {
                            @Override
                            public void onGetUser(UserObject userObject) {
                                USEROBJECT = userObject;

                                setLocationAdapter();
                            }
                        });
                        resumeAddLocationDialog.show(fm, "fragment_edit_name");
                    }




                }

            });
        }
    }
    @OnClick(R.id.btAdd)
    public void Add()
    {

        FragmentManager fm = getActivity().getSupportFragmentManager();
        ResumeAddLocationDialog resumeAddLocationDialog = new ResumeAddLocationDialog(ADDRESOBJECT, USEROBJECT, new ResumeAddLocationDialog.onGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {
                USEROBJECT = userObject;
                setLocationAdapter();

            }
        });

        resumeAddLocationDialog.show(fm, "fragment_edit_name");
    }

    boolean bRelative = false;
    @OnClick(R.id.btList)
    public void ShowList()
    {
        if (!bRelative)
        {
            rlListLocations.setVisibility(View.VISIBLE);
            bRelative = true;
        }
        else
        {
            rlListLocations.setVisibility(View.INVISIBLE);
            bRelative = false;
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_CAMARE_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted) getActivity().finish();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }

    private void setLocation(LatLng latLng)
    {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {

                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng)      // Sets the center of the map to location user
                        .zoom(17)                   // Sets the zoom
                        .bearing(0)                // Sets the orientation of the camera to east
                        .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }


    List<Marker> markerOptionss = new ArrayList<>();
    private Marker addMarket(LatLng latLng, String title)
    {
        MarkerOptions markerOptions = new MarkerOptions();
        Marker marker = map.addMarker(markerOptions
                .position(latLng)
                .title(title));

        markerOptionss.add(marker);

        return marker;
    }

    private void RemoveAllMarkets()
    {
        for (Marker marker : markerOptionss)
        {
            marker.remove();
        }

        markerOptionss = new ArrayList<>();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        else
        {
            //getLastKnownLocation();
            Location location = getLastKnownLocation();
            if (location != null)
            {
                setLocation(new LatLng(location.getLatitude(), location.getLongitude()));

                RemoveAllMarkets();
                addMarket(new LatLng(location.getLatitude(), location.getLongitude()), "Mi hubicacion");

            }
        }

    }

    //Location myLocation = getLastKnownLocation();

    private Location getLastKnownLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        //mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}
