package co.com.rapxy.rapxy.NETWORK.BL;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.CreditCardObject;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.RolObject;
import co.com.rapxy.rapxy.MODAL.StateObjetc;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.WS.GetObjectClass;
import co.com.rapxy.rapxy.NETWORK.WS.SetObjectClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.FIREURL_USERS;


/**
 * Created by pedrodaza on 16/01/18.
 */

public class SetUserClass {

    public interface onGetUser
    {
        void onGetUser(UserObject userObject);
    }

    public void createUserKeyForEmailOridNumber(final UserObject userObject, final onGetUser listener)
    {
        new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(FIREURL_USERS, "idNumber", userObject.getIdNumber(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {


                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    UserObject userObject1 = snapshot1.getValue(UserObject.class);
                    userObject1.setId(snapshot.getKey());
                    listener.onGetUser(userObject1);
                    break;
                }

            }

            @Override
            public void onGetNullObject() {
                new SetObjectClass().SetObjectKeyForAutoKey(userObject, FIREURL_USERS, new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {
                        UserObject userObject1 = snapshot.getValue(UserObject.class);

                        userObject1.setId(snapshot.getKey());
                        listener.onGetUser(userObject1);
                    }

                    @Override
                    public void onGetError(String Err) {

                    }
                });
            }

            @Override
            public void OnGetError(String Err) {

            }
        });

    }

    public void createUserKeyForUid(final Context context, final UserObject userObject, final onGetUser listener)
    {
        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(FIREURL_USERS, "uid", userObject.getUid(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {


                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    UserObject userObject1 = snapshot1.getValue(UserObject.class);
                    userObject1.setId(snapshot1.getKey());
                    listener.onGetUser(userObject1);
                    break;
                }

            }

            @Override
            public void onGetNullObject() {
                new SetObjectClass().SetObjectKeyForAutoKey(userObject, FIREURL_USERS, new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {
                        UserObject userObject1 = snapshot.getValue(UserObject.class);

                        userObject1.setId(snapshot.getKey());
                        listener.onGetUser(userObject1);
                    }

                    @Override
                    public void onGetError(String Err) {
                        new AlertsClass().showAlerInfo(context, "Información", Err);
                        new AlertsClass().hideProggress(progressDialog);
                    }
                });
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, "Información", Err);
                new AlertsClass().hideProggress(progressDialog);

            }
        });

    }

    public void createUserPhotoKeyForEmailOridNumber(final UserObject userObject, ImageView image, final onGetUser listener)
    {
        new UploadImageClass().UploadImageUser(userObject, image, new UploadImageClass.OnSetDataImage() {
            @Override
            public void OnSetImage(String UrlDowload) {

                final String urlDowload = UrlDowload;
                //userObject.setUrlPhoto(urlDowload);
                new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(FIREURL_USERS, "idNumber", userObject.getIdNumber(), new GetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot, String key) {


                        for (DataSnapshot snapshot1 : snapshot.getChildren())
                        {
                            UserObject userObject1 = snapshot1.getValue(UserObject.class);
                            userObject1.setId(snapshot.getKey());
                            listener.onGetUser(userObject1);
                            break;
                        }

                    }

                    @Override
                    public void onGetNullObject() {
                        new SetObjectClass().SetObjectKeyForAutoKey(userObject, FIREURL_USERS, new SetObjectClass.onGetObject() {
                            @Override
                            public void onGetObject(DataSnapshot snapshot) {
                                UserObject userObject1 = snapshot.getValue(UserObject.class);

                                userObject1.setId(snapshot.getKey());
                                listener.onGetUser(userObject1);
                            }

                            @Override
                            public void onGetError(String Err) {

                            }
                        });
                    }

                    @Override
                    public void OnGetError(String Err) {

                    }
                });


            }
        });

    }

    public void SetUserForid(final UserObject userObject, final onGetUser listener)
    {

        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);

            }

            @Override
            public void onGetError(String Err) {

            }
        });



    }

    public  void addBitmapUserForId(Bitmap bitmap,final UserObject userObject, final onGetUser listener)
    {
        new UploadImageClass().UploadBitmapUser(userObject, bitmap, new UploadImageClass.OnSetDataImage() {
            @Override
            public void OnSetImage(String UrlDowload) {

                List<String> strings = userObject.getUrlsDocuments();

                if(strings == null)
                {
                    strings = new ArrayList<>();
                    strings.add(UrlDowload);
                }
                else
                {
                    strings.add(UrlDowload);
                }

                userObject.setUrlsDocuments(strings);

                new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {

                            UserObject userObject1 = snapshot.getValue(UserObject.class);
                            userObject1.setId(snapshot.getKey());
                            listener.onGetUser(userObject1);


                    }

                    @Override
                    public void onGetError(String Err) {

                    }
                });

            }
        });
    }

    public  void addBitmapPhotosUserForId(Bitmap bitmap,final UserObject userObject, final onGetUser listener)
    {
        new UploadImageClass().UploadBitmapPhotosUser(userObject, bitmap, new UploadImageClass.OnSetDataImage() {
            @Override
            public void OnSetImage(String UrlDowload) {

                List<String> strings = userObject.getUrlsPhotos();

                if(strings == null)
                {
                    strings = new ArrayList<>();
                    strings.add(UrlDowload);
                }
                else
                {
                    strings.add(UrlDowload);
                }

                userObject.setUrlsPhotos(strings);

                new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {

                        UserObject userObject1 = snapshot.getValue(UserObject.class);
                        userObject1.setId(snapshot.getKey());
                        listener.onGetUser(userObject1);


                    }

                    @Override
                    public void onGetError(String Err) {

                    }
                });

            }
        });
    }

    public void addAdreesUserForId(UserObject userObject, AddresObject addresObject, final onGetUser listener)
    {
        if (userObject.getAddres() == null)
        {
            List<AddresObject> addresObjects = new ArrayList<>();
            addresObjects.add(addresObject);
            userObject.setAddres(addresObjects);

        }
        else
        {
            List<AddresObject> addresObjects = userObject.getAddres();
            addresObjects.add(addresObject);
            userObject.setAddres(addresObjects);
        }


        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);

            }

            @Override
            public void onGetError(String Err) {

            }
        });
    }

    public void removeUrlImageForId(Context context, boolean bDocument, UserObject userObject, String urlImage, final onGetUser listener)
    {
        if(bDocument)
        {
            if (userObject.getUrlsDocuments() != null)
            {
                int i = 0;
                for (String url : userObject.getUrlsDocuments())
                {
                    if (url.equals(urlImage))
                    {
                        userObject.getUrlsDocuments().remove(url);
                        break;
                    }
                    i++;
                }

                new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {
                        UserObject userObject1 = snapshot.getValue(UserObject.class);
                        userObject1.setId(snapshot.getKey());
                        listener.onGetUser(userObject1);

                    }

                    @Override
                    public void onGetError(String Err) {

                    }
                });

            }
            else
            {

            }
        }
        else
        {
            if (userObject.getUrlsPhotos() != null)
            {
                int i = 0;
                for (String url : userObject.getUrlsPhotos())
                {
                    if (url.equals(urlImage))
                    {
                        userObject.getUrlsPhotos().remove(url);
                        break;
                    }
                    i++;
                }

                new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {
                        UserObject userObject1 = snapshot.getValue(UserObject.class);
                        userObject1.setId(snapshot.getKey());
                        listener.onGetUser(userObject1);

                    }

                    @Override
                    public void onGetError(String Err) {

                    }
                });

            }
            else
            {

            }
        }

    }

    public void updateUserState(Context context, UserObject userObject, List<StateObjetc> stateObjetcs , final onGetUser listener)
    {
        StateObjetc stateObjetcActivo = new StateObjetc();
        StateObjetc stateObjetcDesactivado = new StateObjetc();

        for (StateObjetc stateObjetc : stateObjetcs)
        {
            if(stateObjetc.getValue().equals("AS"))
            {
                stateObjetcActivo = stateObjetc;
            }
            else if (stateObjetc.getValue().equals("DS"))
            {
                stateObjetcDesactivado = stateObjetc;
            }
        }

        if (userObject.getState() == null)
        {
            userObject.setState(stateObjetcActivo);
        }
        else if (userObject.getState().getValue().equals("AS"))
        {
            userObject.setState(stateObjetcDesactivado);
        }
        else
        {
            userObject.setState(stateObjetcActivo);
        }

        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);

            }

            @Override
            public void onGetError(String Err) {

            }
        });


    }

    public void addAndRemoveFavoriteUserForId(Context context, UserObject userObject, UserObject userObjectFavorite, final onGetUser listener)
    {
        if (userObject.getFavorites() == null)
        {
            List<String> strings = new ArrayList<>();
            strings.add(userObjectFavorite.getId());

            userObject.setFavorites(strings);
        }
        else
        {
            int i = 0;
            boolean fExiste = false;
            for (String idfavorite : userObject.getFavorites())
            {
                if (idfavorite.equals(userObjectFavorite.getId()))
                {
                    userObject.getFavorites().remove(i);
                    fExiste = true;
                    break;
                }
                i++;
            }

            if (!fExiste)
            {
                userObject.getFavorites().add(userObjectFavorite.getId());
            }
        }


        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);

            }

            @Override
            public void onGetError(String Err) {

            }
        });
    }

    public void addPlanUser(final Context context, PlanObject planObject, UserObject userObject, final onGetUser listener)
    {

        if (userObject.getPlan() != null)
        {
            userObject.getPlan().add(planObject);
        }
        else
        {
            List<PlanObject> planObjects = new ArrayList<>();
            planObjects.add(planObject);
            userObject.setPlan(planObjects);
        }
        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                new AlertsClass().hideProggress(progressDialog);
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);


            }

            @Override
            public void onGetError(String Err) {

                new AlertsClass().showAlerInfo(context, "Información", Err);
                new AlertsClass().hideProggress(progressDialog);
            }
        });

    }

    public void removePlanUserForId(final Context context, PlanObject planObject, UserObject userObject, final onGetUser listener)
    {
        if (userObject.getPlan() != null)
        {
            userObject.getPlan().remove(planObject);
        }
        else
        {
            new AlertsClass().showAlerInfo(context, "Información", "No tienes planes agregados");
        }
        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                new AlertsClass().hideProggress(progressDialog);
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);


            }

            @Override
            public void onGetError(String Err) {

                new AlertsClass().showAlerInfo(context, "Información", Err);
                new AlertsClass().hideProggress(progressDialog);
            }
        });
    }

    public void editPlanUserForId(final Context context, PlanObject planObject, UserObject userObject, final onGetUser listener)
    {
        if (userObject.getPlan() != null)
        {

            for (PlanObject planObject1 : userObject.getPlan())
            {
                if(planObject.getId().equals(planObject1.getId()))
                {
                    planObject1.setPrice(planObject.getPrice());
                }
            }
        }
        else
        {
            new AlertsClass().showAlerInfo(context, "Información", "No tienes planes agregados");
        }
        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                new AlertsClass().hideProggress(progressDialog);
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);


            }

            @Override
            public void onGetError(String Err) {

                new AlertsClass().showAlerInfo(context, "Información", Err);
                new AlertsClass().hideProggress(progressDialog);
            }
        });
    }

    public void setRolUserForId(final Context context, UserObject userObject, RolObject rolObject, final onGetUser listener)
    {

        userObject.setRol(rolObject);
        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                new AlertsClass().hideProggress(progressDialog);
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);


            }

            @Override
            public void onGetError(String Err) {

                new AlertsClass().showAlerInfo(context, "Información", Err);
                new AlertsClass().hideProggress(progressDialog);
            }
        });
    }

    public void addCreditCardUserForId(final Context context, UserObject userObject, CreditCardObject creditCardObject, final onGetUser listener)
    {
        if (userObject.getCreditCards() == null)
        {
            List<CreditCardObject> creditCardObjects = new ArrayList<>();
            creditCardObjects.add(creditCardObject);

            userObject.setCreditCards(creditCardObjects);
        }
        else
        {
            userObject.getCreditCards().add(creditCardObject);
        }

        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                new AlertsClass().hideProggress(progressDialog);
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);


            }

            @Override
            public void onGetError(String Err) {

                new AlertsClass().showAlerInfo(context, "Información", Err);
                new AlertsClass().hideProggress(progressDialog);
            }
        });
    }

    public void removeCreditCardUserForId(final Context context, UserObject userObject, CreditCardObject creditCardObject, final onGetUser listener)
    {
        if (userObject.getCreditCards() == null)
        {
            new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_item_vacio));
        }
        else
        {
            userObject.getCreditCards().remove(creditCardObject);
        }

        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new SetObjectClass().SetObjectKeyForCustomKey(userObject, FIREURL_USERS, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {
                new AlertsClass().hideProggress(progressDialog);
                UserObject userObject1 = snapshot.getValue(UserObject.class);
                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);


            }

            @Override
            public void onGetError(String Err) {

                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
                new AlertsClass().hideProggress(progressDialog);
            }
        });
    }


}
