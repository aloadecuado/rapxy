package co.com.rapxy.rapxy.NETWORK.CUSTOMSERVICES;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import co.com.rapxy.rapxy.MODAL.AddresObject;
import co.com.rapxy.rapxy.MODAL.PlanObject;
import co.com.rapxy.rapxy.MODAL.ServicesObject;
import co.com.rapxy.rapxy.MODAL.UserObject;
import co.com.rapxy.rapxy.NETWORK.BL.GetServicesClass;
import co.com.rapxy.rapxy.NETWORK.BL.SetServiceClass;
import co.com.rapxy.rapxy.R;
import co.com.rapxy.rapxy.UI.DIALOG.PayTravelDialog;
import co.com.rapxy.rapxy.UI.DIALOG.TravelVehiculoDialog;
import co.com.rapxy.rapxy.UTIL.AlertsClass;

import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.SERVICEOBJECT;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECT;
import static co.com.rapxy.rapxy.UTIL.GlobalConstansAndVarClass.USEROBJECTSERVICE;

public class InmediateTravelServices {

    OnGetServices onGetServices;
    public InmediateTravelServices(OnGetServices onGetServices){
        this.onGetServices = onGetServices;
    }
    public interface OnGetServices{
        void onServices(List<ServicesObject> servicesObjects);
        void onPeddingServices(ServicesObject servicesObject);
        void onCounterProposalService(ServicesObject servicesObject);
        void onAcepptedServices(ServicesObject servicesObject);
        void onArrivedServices(ServicesObject servicesObject);
        void onFinishServices(ServicesObject servicesObject);
        void onQualifiedServices(ServicesObject servicesObject);
        void onPayProcecedServices(ServicesObject servicesObject);
        void onCancelServices(ServicesObject servicesObject);
    }

    public void addServicesImediate(Context context, UserObject userObjectCliente, UserObject userObjectVehiculo, AddresObject addresObject, PlanObject planObject){


        final ServicesObject servicesObject = new ServicesObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        Date convertedDate = new Date();
        String strDate = dateFormat.format(convertedDate);

        servicesObject.setAddress(addresObject);
        servicesObject.setDateRegisterString(strDate);
        servicesObject.setPlan(planObject);
        servicesObject.setIdDriver(userObjectVehiculo.getId());
        servicesObject.setIdUser(userObjectCliente.getId());
        servicesObject.setLat(addresObject.getLat());
        servicesObject.setLot(addresObject.getLog());
        new AlertsClass().showAlerInfoYesOrNo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_message_send_prospa), new AlertsClass.onGetData() {
            @Override
            public void onGetDataOk() {
                new SetServiceClass().addServiceImnediateForUserAndVehiculo(context, servicesObject, new SetServiceClass.onGetService() {
                    @Override
                    public void onGetService(ServicesObject servicesObject) {

                        onGetServices.onPeddingServices(servicesObject);
                        /*SERVICEOBJECT = servicesObject;

                        FragmentManager fragmentManager = getFragmentManager();
                        TravelVehiculoDialog travelUserDialog = new TravelVehiculoDialog(SERVICEOBJECT,USEROBJECT, USEROBJECTSERVICE, new TravelVehiculoDialog.onGetUserAddresVehiculo() {
                            @Override
                            public void onGetUser(ServicesObject servicesObject) {
                                SERVICEOBJECT = servicesObject;
                            }

                            @Override
                            public void onGetPayService(ServicesObject servicesObject) {

                                SERVICEOBJECT = servicesObject;
                                FragmentManager fragmentManager1 = getFragmentManager();
                                PayTravelDialog payTravelDialog = new PayTravelDialog(SERVICEOBJECT, USEROBJECT, new PayTravelDialog.onGetService() {
                                    @Override
                                    public void onGetService(ServicesObject servicesObject) {
                                        SERVICEOBJECT = servicesObject;
                                    }
                                });

                                payTravelDialog.show(fragmentManager1, "PayTravelDialog");

                            }
                        });
                        travelUserDialog.show(fragmentManager, "TravelUserDialog");*/
                    }
                });

            }

            @Override
            public void onGetDataCalcel() {

            }
        });
    }


    public void suscribeServices(Context context, final UserObject userObject){



        new GetServicesClass().getAllSercices(context, userObject, new GetServicesClass.onGetServicesUser() {
            @Override
            public void onGetServiceUser(List<ServicesObject> servicesObject) {

                onGetServices.onServices(servicesObject);
                for(ServicesObject servicesObject1 : servicesObject){
                    new GetServicesClass().subscribeAllServiceForId(context, servicesObject1, new GetServicesClass.onGetService() {
                        @Override
                        public void onGetService(ServicesObject servicesObject) {
                            if(servicesObject.getState() != null){
                                if(servicesObject.getState().getValue().equals("ES")){
                                    onGetServices.onPeddingServices(servicesObject);
                                }else if(servicesObject.getState().getValue().equals("TS")){
                                    onGetServices.onAcepptedServices(servicesObject);
                                }else if(servicesObject.getState().getValue().equals("CS")){
                                    onGetServices.onCounterProposalService(servicesObject);
                                }else if(servicesObject.getState().getValue().equals("PPS")){
                                    onGetServices.onPayProcecedServices(servicesObject);
                                }else if(servicesObject.getState().getValue().equals("VLS")){
                                    onGetServices.onArrivedServices(servicesObject);
                                }else if(servicesObject.getState().getValue().equals("FS")){
                                    onGetServices.onFinishServices(servicesObject);
                                }else if(servicesObject.getState().getValue().equals("LS")){
                                    onGetServices.onQualifiedServices(servicesObject);
                                }else if(servicesObject.getState().getValue().equals("NS")){
                                    onGetServices.onCancelServices(servicesObject);
                                }
                            }
                        }

                        @Override
                        public void onNoService() {

                        }
                    });
                }
            }

            @Override
            public void onNoService() {

                ArrayList<ServicesObject> servicesObject = new ArrayList<ServicesObject>();
                onGetServices.onServices(servicesObject);
            }
        });
    }




}
